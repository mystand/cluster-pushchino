# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170209123428) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "activity_types", force: :cascade do |t|
    t.jsonb    "name",       default: {}
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 191, null: false
    t.integer  "sluggable_id",               null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 191
    t.datetime "created_at",                 null: false
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "group_users", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "group_users", ["group_id", "user_id"], name: "index_group_users_on_group_id_and_user_id", unique: true, using: :btree
  add_index "group_users", ["user_id"], name: "index_group_users_on_user_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "title",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "imperavi_attachments", force: :cascade do |t|
    t.string   "file_uid"
    t.string   "file_name"
    t.string   "target_type"
    t.integer  "target_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "imperavi_attachments", ["target_id", "target_type"], name: "index_imperavi_attachments_on_target_id_and_target_type", using: :btree

  create_table "imperavi_pictures", force: :cascade do |t|
    t.string   "file_uid"
    t.string   "file_name"
    t.string   "target_type"
    t.integer  "target_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "imperavi_pictures", ["target_id", "target_type"], name: "index_imperavi_pictures_on_target_id_and_target_type", using: :btree

  create_table "interviews", force: :cascade do |t|
    t.jsonb    "title",        default: {}
    t.jsonb    "intro",        default: {}
    t.string   "image_uid"
    t.string   "image_name"
    t.jsonb    "content",      default: {}
    t.date     "publish_date"
    t.boolean  "published"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "url"
  end

  add_index "interviews", ["published"], name: "index_interviews_on_published", using: :btree
  add_index "interviews", ["url"], name: "index_interviews_on_url", unique: true, using: :btree

  create_table "mailing_attachments", force: :cascade do |t|
    t.string   "file_uid"
    t.integer  "mailing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailings", force: :cascade do |t|
    t.string   "status",       default: "new"
    t.text     "body"
    t.text     "title"
    t.json     "sending_data"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "signature"
  end

  create_table "mailings_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "mailing_id"
  end

  create_table "menu_items", force: :cascade do |t|
    t.text     "children_ids",  default: [],                 array: true
    t.integer  "parent_id"
    t.integer  "target_id"
    t.string   "target_type"
    t.string   "url"
    t.string   "key"
    t.boolean  "root",          default: false
    t.boolean  "published",     default: true
    t.boolean  "breadcrumbs",   default: true
    t.jsonb    "title",         default: {},    null: false
    t.jsonb    "annotation",    default: {}
    t.string   "icon_uid"
    t.boolean  "show_children", default: true
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "semantic_url"
  end

  add_index "menu_items", ["parent_id", "published"], name: "index_menu_items_on_parent_id_and_published", using: :btree
  add_index "menu_items", ["parent_id"], name: "index_menu_items_on_parent_id", using: :btree
  add_index "menu_items", ["semantic_url"], name: "index_menu_items_on_semantic_url", unique: true, using: :btree
  add_index "menu_items", ["target_id", "target_type"], name: "index_menu_items_on_target_id_and_target_type", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "topic_id"
    t.string   "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "editor_id"
  end

  add_index "messages", ["topic_id"], name: "index_messages_on_topic_id", using: :btree
  add_index "messages", ["user_id", "topic_id"], name: "index_messages_on_user_id_and_topic_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "options", force: :cascade do |t|
    t.jsonb    "title",       default: {}
    t.jsonb    "description", default: {}
    t.integer  "vote_id",                  null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "options", ["vote_id"], name: "index_options_on_vote_id", using: :btree

  create_table "organization_activity_types", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "activity_type_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "organization_activity_types", ["activity_type_id", "organization_id"], name: "organization_activity_types_main_index", unique: true, using: :btree
  add_index "organization_activity_types", ["organization_id"], name: "index_organization_activity_types_on_organization_id", using: :btree

  create_table "organization_attachments", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "file_uid"
    t.text     "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "organization_attachments", ["organization_id"], name: "index_organization_attachments_on_organization_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.jsonb     "name",                                                                     default: {},    null: false
    t.datetime  "created_at",                                                                               null: false
    t.datetime  "updated_at",                                                                               null: false
    t.string    "logo_uid"
    t.jsonb     "chief_name",                                                               default: {}
    t.jsonb     "competence",                                                               default: {}
    t.jsonb     "capabilities",                                                             default: {}
    t.jsonb     "contacts",                                                                 default: {}
    t.geography "coordinates",  limit: {:srid=>4326, :type=>"geometry", :geographic=>true}
    t.boolean   "in_slider",                                                                default: false
    t.string    "website"
    t.jsonb     "address"
    t.string    "url"
    t.jsonb     "offers"
    t.jsonb     "interests"
  end

  add_index "organizations", ["in_slider"], name: "index_organizations_on_in_slider", using: :btree
  add_index "organizations", ["name"], name: "index_organizations_on_name", using: :gin
  add_index "organizations", ["url"], name: "index_organizations_on_url", unique: true, using: :btree

  create_table "organizations_users", id: false, force: :cascade do |t|
    t.integer "organization_id", null: false
    t.integer "user_id",         null: false
  end

  add_index "organizations_users", ["organization_id", "user_id"], name: "index_organizations_users_on_organization_id_and_user_id", using: :btree

  create_table "page_fragments", force: :cascade do |t|
    t.string   "name",                    null: false
    t.string   "key",                     null: false
    t.jsonb    "content",    default: {}
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "pages", force: :cascade do |t|
    t.jsonb    "title",      default: {}
    t.jsonb    "intro",      default: {}
    t.jsonb    "content",    default: {}
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "url"
  end

  add_index "pages", ["url"], name: "index_pages_on_url", unique: true, using: :btree

  create_table "pictures", force: :cascade do |t|
    t.string   "target_type"
    t.integer  "target_id"
    t.string   "file_uid"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  add_index "pictures", ["target_id", "target_type"], name: "index_pictures_on_target_id_and_target_type", using: :btree

  create_table "posts", force: :cascade do |t|
    t.jsonb    "title",        default: {}
    t.jsonb    "intro",        default: {}
    t.string   "image_uid"
    t.string   "image_name"
    t.jsonb    "content",      default: {}
    t.date     "publish_date"
    t.boolean  "published"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "approved"
    t.integer  "user_id"
    t.string   "url"
  end

  add_index "posts", ["published", "approved"], name: "index_posts_on_published_and_approved", using: :btree
  add_index "posts", ["published"], name: "index_posts_on_published", using: :btree
  add_index "posts", ["url"], name: "index_posts_on_url", unique: true, using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "presentation_slides", force: :cascade do |t|
    t.jsonb    "title",           default: {}
    t.jsonb    "text",            default: {}
    t.integer  "order_number"
    t.integer  "presentation_id"
    t.string   "image_name"
    t.string   "image_uid"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "presentation_slides", ["presentation_id"], name: "index_presentation_slides_on_presentation_id", using: :btree

  create_table "presentations", force: :cascade do |t|
    t.jsonb    "name",       default: {}
    t.string   "url"
    t.boolean  "published"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "presentations", ["published"], name: "index_presentations_on_published", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "contact_phone_number"
    t.string   "contact_email"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.jsonb    "site_name",                      default: {}
    t.jsonb    "greeting_title"
    t.text     "greeting_image_uid"
    t.jsonb    "greeting_paragraph_1"
    t.jsonb    "greeting_paragraph_2"
    t.jsonb    "greeting_short_description"
    t.jsonb    "user_notification_user_ids"
    t.jsonb    "vote_notification_user_ids"
    t.string   "ig_url"
    t.string   "vk_url"
    t.string   "fb_url"
    t.string   "ln_url"
    t.jsonb    "feedback_notification_user_ids"
    t.text     "default_signature"
  end

  create_table "slides", force: :cascade do |t|
    t.jsonb    "title",      default: {}
    t.jsonb    "subtitle",   default: {}
    t.string   "image_uid"
    t.boolean  "published",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "slides", ["published"], name: "index_slides_on_published", using: :btree

  create_table "topics", force: :cascade do |t|
    t.string   "title",                           null: false
    t.boolean  "is_archieved",    default: false
    t.integer  "user_id"
    t.string   "topic_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.datetime "last_message_at"
  end

  add_index "topics", ["is_archieved", "user_id", "topic_type"], name: "index_topics_on_is_archieved_and_user_id_and_topic_type", using: :btree
  add_index "topics", ["is_archieved"], name: "index_topics_on_is_archieved", using: :btree
  add_index "topics", ["topic_type"], name: "index_topics_on_topic_type", using: :btree
  add_index "topics", ["user_id"], name: "index_topics_on_user_id", using: :btree

  create_table "topics_users", id: false, force: :cascade do |t|
    t.integer "topic_id", null: false
    t.integer "user_id",  null: false
  end

  add_index "topics_users", ["topic_id", "user_id"], name: "index_topics_users_on_topic_id_and_user_id", using: :btree
  add_index "topics_users", ["user_id"], name: "index_topics_users_on_user_id", using: :btree

  create_table "user_options", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "option_id"
    t.integer  "organization_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "vote_id"
  end

  add_index "user_options", ["option_id"], name: "index_user_options_on_option_id", using: :btree
  add_index "user_options", ["organization_id"], name: "index_user_options_on_organization_id", using: :btree
  add_index "user_options", ["user_id", "option_id", "organization_id", "vote_id"], name: "user_options_main_index", using: :btree
  add_index "user_options", ["user_id"], name: "index_user_options_on_user_id", using: :btree
  add_index "user_options", ["vote_id"], name: "index_user_options_on_vote_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "role",                   default: "guest", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "post"
    t.integer  "organization_id"
    t.boolean  "approved",               default: false
    t.string   "outer_organization"
    t.string   "image_uid"
  end

  add_index "users", ["approved", "organization_id"], name: "index_users_on_approved_and_organization_id", using: :btree
  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vote_groups", force: :cascade do |t|
    t.integer  "vote_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "vote_groups", ["group_id", "vote_id"], name: "index_vote_groups_on_group_id_and_vote_id", unique: true, using: :btree
  add_index "vote_groups", ["vote_id"], name: "index_vote_groups_on_vote_id", using: :btree

  create_table "vote_users", force: :cascade do |t|
    t.integer  "vote_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "votes", force: :cascade do |t|
    t.jsonb    "title",                    default: {}
    t.jsonb    "description",              default: {}
    t.date     "date_start",                               null: false
    t.date     "date_end",                                 null: false
    t.boolean  "published",                default: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "show_results_immediately", default: false
    t.boolean  "allow_multiple_answer",    default: false
    t.integer  "organizations_count",      default: 0
    t.json     "results"
    t.boolean  "closed",                   default: false
  end

  add_index "votes", ["closed"], name: "index_votes_on_closed", using: :btree
  add_index "votes", ["published", "closed"], name: "index_votes_on_published_and_closed", using: :btree
  add_index "votes", ["published"], name: "index_votes_on_published", using: :btree

end
