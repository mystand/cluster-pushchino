if Organization.count.zero?
  admins_organization = Organization.create! name: "admins"
  puts "Admins organization successfully created".green
else
  admins_organization = Organization.first
end

unless User.where(email: 'admin@mystand.ru').any?
  user = User.create!(email: 'admin@mystand.ru',
                      organization: admins_organization,
                      first_name: "Admin", 
                      middle_name: "Admin",
                      last_name: "ADmin",
                      post: "admin",
                      password: 'beer4stepa',
                      password_confirmation: 'beer4stepa',
                      role: 'admin')

  puts "User ".green + "#{user.email} #{user.password} " + "successfully created".green
end

if Settings.count.zero?
  if Settings.create(
    contact_phone_number: '+7 (495) 280-79-84',
    contact_email: 'cluster@mosregco.ru',
    site_name: {
      ru: 'Кластер Пущино',
      en: 'Cluster Pushchino'
    }
  )
    puts "Settings successfully created".colorize(:green)
  end
end

if MenuItem.count.zero?
  root = MenuItem.create! title: "Root", root: true
else
  root = MenuItem.root
end

if MenuItem.count < 3
  main = MenuItem.create! key: "main", title: I18n.t("application.navigation.main"), parent: root
  footer = MenuItem.create! key: "footer", title: I18n.t("application.navigation.footer"), parent: root
  root.children_ids = [main.id, footer.id]
  root.save
end

# PageFragment
unless PageFragment.where(key: "main_page_about").any?
  PageFragment.create!(
    name: 'О кластере (главная страница)',
    key: 'main_page_about',
    content: {
      ru: '
<h2>О кластере</h2>
<p>ИТК - совокупность размещенных на географически ограниченной территории организаций-участников кластера, которая характеризуется:</p>
<ul>
<li>Объединяющей участников кластера научно-производственной цепочки в одной или нескольких направлениях деятельности;</li>
<li>Наличием специализированной организации осуществляющей координацию деятельности кластера и его участников;</li>
<li>Синергетическим эффектом, который выражается в повышении экономической эффективности и результативности деятельности каждого предприятия или организации кластера;</li>
</ul>
<p><a href="#">Стать резидентом</a></p>
      ',
      en: ''
    }
  )
else
  puts "PageFragment with key `main_page_about` already exists".colorize(:yellow)
end

# PageFragment
unless PageFragment.where(key: "contacts").any?
  PageFragment.create!(
    name: 'Контакты',
    key: 'contacts',
    content: {
      ru: '<h1>Контакты</h1><p><br></p><p><strong>rumyanceva@psn.ru</strong></p><p><strong><br></strong></p><p><strong>(495) 632-78-68</strong></p><p><strong><br></strong></p><p><strong>(4967) 73-26-36</strong></p>',
      en: '<h1>Contacts</h1><p><br></p><p><strong>rumyanceva@psn.ru</strong></p><p><strong><br></strong></p><p><strong>+7-495-632-78-68</strong></p><p><strong><br></strong></p><p><strong>+7-4967-73-26-36</strong></p>'
    }
  )
else
  puts "PageFragment with key `contacts` already exists".colorize(:yellow)
end

# PageFragment
unless PageFragment.where(key: "footer_contacts").any?
  PageFragment.create!(
      name: 'Контакты в футере',
      key: 'footer_contacts',
      content: {
          ru: '<p><a href="mailto:rumyanceva@psn.ru">rumyanceva@psn.ru</a></p><p>	(495) 632-78-68</p><p>	(4967) 73-26-36</p>',
          en: '<p><a href="mailto:rumyanceva@psn.ru">rumyanceva@psn.ru</a></p><p>	(495) 632-78-68</p><p>	(4967) 73-26-36</p>'
      }
  )
else
  puts "PageFragment with key `footer_contacts` already exists".colorize(:yellow)
end