class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :description, default: '{}'
      t.integer :vote_id, null: false

      t.timestamps null: false
    end
  end
end
