class AddUrlToPages < ActiveRecord::Migration
  def up
    add_column :pages, :url, :string
    add_index :pages, :url, unique: true
    Page.initialize_urls
  end
  def down
    remove_column :pages, :url
  end
end
