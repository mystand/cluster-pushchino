class CreateActivityTypes < ActiveRecord::Migration
  def change
    create_table :activity_types do |t|
      t.jsonb :name, default: '{}'

      t.timestamps null: false
    end
  end
end
