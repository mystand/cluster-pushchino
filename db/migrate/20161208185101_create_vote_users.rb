class CreateVoteUsers < ActiveRecord::Migration
  def change
    create_table :vote_users do |t|
      t.integer :vote_id
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
