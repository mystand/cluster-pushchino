class AddOuterOrganizationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :outer_organization, :string, null: true
  end
end
