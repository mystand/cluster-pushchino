class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :intro, default: '{}'
      t.jsonb :content, default: '{}'

      t.timestamps null: false
    end
  end
end
