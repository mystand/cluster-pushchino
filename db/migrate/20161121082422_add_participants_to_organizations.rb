class AddParticipantsToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :participants, :geometry, geographic: true
  end
end
