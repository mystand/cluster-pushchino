class CreateInterviews < ActiveRecord::Migration
  def change
    create_table :interviews do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :intro, default: '{}'
      t.string :image_uid
      t.string :image_name
      t.jsonb :content, default: '{}'
      t.date :publish_date
      t.boolean :published

      t.timestamps null: false
    end
  end
end
