class CreateOrganizationAttachments < ActiveRecord::Migration
  def change
    create_table :organization_attachments do |t|
      t.integer :organization_id
      t.string :file_uid
      t.text :description
      t.timestamps null: false
    end
  end
end
