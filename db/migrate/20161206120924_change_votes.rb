class ChangeVotes < ActiveRecord::Migration
  def change
    add_column :votes, :organizations_count, :integer, default: 0
    add_column :votes, :results, :json
  end
end
