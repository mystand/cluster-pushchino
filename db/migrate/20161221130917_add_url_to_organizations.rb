class AddUrlToOrganizations < ActiveRecord::Migration
  def up
    add_column :organizations, :url, :string
    add_index :organizations, :url, unique: true
    Organization.initialize_urls
  end
  def down
    remove_column :organizations, :url
  end
end
