class AddAllowMultipleAnswerToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :allow_multiple_answer, :boolean, default: false
  end
end
