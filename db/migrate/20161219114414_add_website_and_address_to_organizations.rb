class AddWebsiteAndAddressToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :website, :string
    add_column :organizations, :address, :jsonb
  end
end
