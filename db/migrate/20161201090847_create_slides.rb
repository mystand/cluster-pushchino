class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :subtitle, default: '{}'
      t.string :image_uid, null: true
      t.boolean :published, default: false

      t.timestamps null: false
    end
  end
end
