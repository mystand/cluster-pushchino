class CreateOrganizationActivityTypes < ActiveRecord::Migration
  def change
    create_table :organization_activity_types do |t|
      t.integer :organization_id
      t.integer :activity_type_id

      t.timestamps null: false
    end
  end
end
