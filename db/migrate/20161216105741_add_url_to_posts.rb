class AddUrlToPosts < ActiveRecord::Migration
  def up
    add_column :posts, :url, :string
    add_index :posts, :url, unique: true

    # You'll want to generate the URLs for all existing users:
    Post.initialize_urls
  end

  def down
    remove_index :posts, :url
    remove_column :posts, :url
  end
end
