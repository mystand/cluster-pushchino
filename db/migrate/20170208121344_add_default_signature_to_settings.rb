class AddDefaultSignatureToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :default_signature, :text
  end
end
