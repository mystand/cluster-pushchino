class ChangeUserOptions < ActiveRecord::Migration
  def change
    rename_column :user_options, :company_id, :organization_id
    add_column :user_options, :vote_id, :integer    
  end
end