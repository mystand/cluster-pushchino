class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title, null: false
      t.boolean :is_archieved, default: false
      t.integer :user_id
      t.string :topic_type

      t.timestamps null: false
    end
    create_join_table :topics, :users do |t|
      t.index [:topic_id, :user_id]
      # t.index [:user_id, :topic_id]
    end
  end
end
