class AddEditorIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :editor_id, :integer
  end
end
