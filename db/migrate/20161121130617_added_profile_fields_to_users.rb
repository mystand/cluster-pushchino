class AddedProfileFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string, null: true
    add_column :users, :middle_name, :string, null: true
    add_column :users, :last_name, :string, null: true
    add_column :users, :post, :string, null: true
    add_column :users, :organization_id, :integer, null: true
  end
end
