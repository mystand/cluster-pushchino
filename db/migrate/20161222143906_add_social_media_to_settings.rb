class AddSocialMediaToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :ig_url, :string
    add_column :settings, :vk_url, :string
    add_column :settings, :fb_url, :string
    add_column :settings, :ln_url, :string
  end
end
