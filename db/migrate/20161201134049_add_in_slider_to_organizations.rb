class AddInSliderToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :in_slider, :boolean, default: false
  end
end
