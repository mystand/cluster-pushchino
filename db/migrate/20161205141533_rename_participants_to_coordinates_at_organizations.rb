class RenameParticipantsToCoordinatesAtOrganizations < ActiveRecord::Migration
  def change
    rename_column :organizations, :participants, :coordinates
  end
end
