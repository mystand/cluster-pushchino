class AddClosedToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :closed, :boolean, default: false
  end
end
