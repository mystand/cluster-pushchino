class AddNotificationUserIdsToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :user_notification_user_ids, :jsonb
    add_column :settings, :vote_notification_user_ids, :jsonb
  end
end
