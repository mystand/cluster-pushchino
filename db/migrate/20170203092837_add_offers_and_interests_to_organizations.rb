class AddOffersAndInterestsToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :offers, :jsonb
    add_column :organizations, :interests, :jsonb
  end
end
