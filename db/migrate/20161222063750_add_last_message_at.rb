class Topic < ActiveRecord::Base
  has_many :messages, autosave: true
end

class Message < ActiveRecord::Base
  belongs_to :topic
end

class AddLastMessageAt < ActiveRecord::Migration
  def up
    add_column :topics, :last_message_at, :datetime

    Topic.all.each do |topic|
      topic.update! last_message_at: topic.messages.maximum(:created_at)
    end
  end

  def down
    remove_column :topics, :last_message_at
  end
end