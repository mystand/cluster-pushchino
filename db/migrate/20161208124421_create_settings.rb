class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :contact_phone_number
      t.string :contact_email

      t.timestamps null: false
    end
  end
end
