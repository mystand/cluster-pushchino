class CreateVoteGroups < ActiveRecord::Migration
  def change
    create_table :vote_groups do |t|
      t.integer :vote_id
      t.integer :group_id

      t.timestamps null: false
    end
  end
end
