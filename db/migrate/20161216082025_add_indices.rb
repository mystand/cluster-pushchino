class AddIndices < ActiveRecord::Migration
  def change
    add_index :group_users, [:group_id, :user_id], unique: true
    add_index :group_users, :user_id

    add_index :imperavi_attachments, [:target_id, :target_type]
    add_index :imperavi_pictures, [:target_id, :target_type]

    add_index :interviews, :published

    add_index :menu_items, :parent_id
    add_index :menu_items, [:parent_id, :published]
    add_index :menu_items, [:target_id, :target_type]

    add_index :messages, :user_id
    add_index :messages, :topic_id
    add_index :messages, [:user_id, :topic_id]

    add_index :options, :vote_id

    add_index :organization_activity_types, [:activity_type_id, :organization_id],
              unique: true, name: :organization_activity_types_main_index
    add_index :organization_activity_types, :organization_id

    add_index :organization_attachments, :organization_id

    add_index :pictures, [:target_id, :target_type]

    add_index :posts, :user_id
    add_index :posts, :published
    add_index :posts, [:published, :approved]

    add_index :presentation_slides, :presentation_id

    add_index :presentations, :published

    add_index :slides, :published

    add_index :topics, :is_archieved
    add_index :topics, :user_id
    add_index :topics, :topic_type
    add_index :topics, [:is_archieved, :user_id, :topic_type]

    add_index :topics_users, :user_id

    add_index :user_options, :user_id
    add_index :user_options, :option_id
    add_index :user_options, :organization_id
    add_index :user_options, :vote_id
    add_index :user_options, [:user_id, :option_id, :organization_id, :vote_id],
              name: :user_options_main_index

    add_index :users, :approved
    add_index :users, :organization_id
    add_index :users, [:approved, :organization_id]

    add_index :vote_groups, [:group_id, :vote_id], unique: true
    add_index :vote_groups, :vote_id

    add_index :votes, :published
    add_index :votes, :closed
    add_index :votes, [:published, :closed]
  end
end