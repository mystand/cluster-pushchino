class CreateMailingAttachments < ActiveRecord::Migration
  def change
    create_table :mailing_attachments do |t|
      t.string :file_uid
      t.integer :mailing_id
      t.timestamps null: false
    end
  end
end
