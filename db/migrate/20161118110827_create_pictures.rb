class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :target_type
      t.integer :target_id
      t.string :file_uid

      t.timestamps null: false
    end
  end
end
