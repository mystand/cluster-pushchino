class CreatePresentations < ActiveRecord::Migration
  def change
    create_table :presentations do |t|
      t.jsonb :name, default: '{}'
      t.string :url
      t.boolean :published

      t.timestamps null: false
    end
  end
end
