class AddUrlToInterviews < ActiveRecord::Migration
  def up
    add_column :interviews, :url, :string
    add_index :interviews, :url, unique: true
    Interview.initialize_urls
  end
  def down
    remove_column :interviews, :url
  end
end
