class AddFeedbackNotificationUserIdsToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :feedback_notification_user_ids, :jsonb
  end
end
