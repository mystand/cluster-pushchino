class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.text :children_ids, default: [], array: true
      t.integer :parent_id
      t.integer :target_id
      t.string :target_type
      t.string :url
      t.string :key
      t.boolean :root, default: false
      t.boolean :published, default: true
      t.boolean :breadcrumbs, default: true
      t.jsonb :title, null: false, default: '{}'
      t.jsonb :annotation, default: '{}'
      t.string :icon_uid
      t.boolean :show_children, default: true

      t.timestamps null: false
    end
  end
end
