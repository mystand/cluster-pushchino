class CreatePresentationSlides < ActiveRecord::Migration
  def change
    create_table :presentation_slides do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :text, default: '{}'
      t.integer :order_number
      t.integer :presentation_id
      t.string :image_name
      t.string :image_uid

      t.timestamps null: false
    end
  end
end
