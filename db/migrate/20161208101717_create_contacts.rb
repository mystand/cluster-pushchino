class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email, required: true
      t.string :name
      t.string :subject
      t.text :body
      t.timestamps
    end
  end
end