class CreatePageFragments < ActiveRecord::Migration
  def change
    create_table :page_fragments do |t|
      t.string :name, null: false
      t.string :key, null: false
      t.jsonb :content, default: '{}'

      t.timestamps null: false
    end
  end
end
