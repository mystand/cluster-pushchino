class AddShowResultsImmediatelyToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :show_results_immediately, :boolean, default: false
  end
end
