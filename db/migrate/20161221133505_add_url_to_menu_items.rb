class AddUrlToMenuItems < ActiveRecord::Migration
  def up
    add_column :menu_items, :semantic_url, :string
    add_index :menu_items, :semantic_url, unique: true
    
    # can't be done like normal with custom field
    urls = MenuItem.all.order(:id).map &:url
    MenuItem.all.order(:id).map {|e| e.update url: nil}
    MenuItem.initialize_urls
    MenuItem.all.order(:id).map {|e| e.update semantic_url: e.url}
    urls.map.with_index {|url,i| MenuItem.all.order(:id).offset(i).limit(1).first.update url: url}
  end
  def down
    remove_column :menu_items, :semantic_url
  end
end
