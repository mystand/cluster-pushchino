class AddSiteNameToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :site_name, :jsonb, default: '{}'
  end
end
