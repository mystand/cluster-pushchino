class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.jsonb :name, null: false, default: '{}'

      t.timestamps null: false
    end

    add_index  :organizations, :name, using: :gin
  end
end
