class AddDeclinedToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :approved, :boolean
    add_column :posts, :user_id, :integer
  end
end