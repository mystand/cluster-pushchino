class AddToSettingsGreeting < ActiveRecord::Migration
  def change
    add_column :settings, :greeting_title, :jsonb
    add_column :settings, :greeting_image_uid, :text
    add_column :settings, :greeting_paragraph_1, :jsonb
    add_column :settings, :greeting_paragraph_2, :jsonb
  end
end