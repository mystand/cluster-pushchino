class CreateMailings < ActiveRecord::Migration
  def change
    create_table :mailings do |t|
      t.string :status, default: 'new'
      t.text :body
      t.text :title
      t.json :sending_data
      t.timestamps null: false      
    end
  end
end
