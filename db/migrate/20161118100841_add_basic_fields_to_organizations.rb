class AddBasicFieldsToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :logo_uid, :string, null: true
    add_column :organizations, :chief_name, :jsonb, default: '{}'
    add_column :organizations, :competence, :jsonb, default: '{}'
    add_column :organizations, :capabilities, :jsonb, default: '{}'
    add_column :organizations, :contacts, :jsonb, default: '{}'
  end
end
