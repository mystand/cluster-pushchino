class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.jsonb :title, default: '{}'
      t.jsonb :description, default: '{}'
      t.date :date_start, null: false
      t.date :date_end, null: false
      t.boolean :published, default: false

      t.timestamps null: false
    end
  end
end
