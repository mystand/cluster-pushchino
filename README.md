## README

### Необходимые зависимости
```
sudo apt-get install libgmp3-dev
```

### Важные шаги
1. Ruby-version: 2.2.5
2. Don't forget about gemset creation 
3. bundle
4. gem install puma
5. Ensure, that mina version is 0.3.8
6. mina staging deploy

### Первый старт
```
cp config/database.yml.example config/database.yml
cp config/secrets.yml.example config/secrets.yml
rake db:create
rake db:migrate
rake db:seed
```

### Multilanguage application
Поддерживаемые языки: английский и русский.
