#!/bin/bash
# usage: ./fetch.sh [staging] [--replace-db]

server=$1
to_replace=$2
localdb=cluster_pushchino_development

cowsay="
______________________________
 Fetching $server database
------------------------------
   \\
    \\
   ^__^         /
   (oo)\_______/  _________
   (__)\       )=(  ____|_ \_____
       ||----w |  \ \     \_____ |
       ||     ||   ||           ||
"

case $server in
  'staging')
    connection=cluster-pushchino@new.mystand.ru
    db=cluster_pushchino_staging
    port=5433
    ;;
  *)
    echo 'No such server!'
    exit 64
esac

if [ "$connection" ]; then
  echo "$cowsay"
  ssh $connection "bash --login -c 'pg_dump --no-owner --no-acl $db -f postgres_${server}.sql -p ${port}'"
  scp $connection:~/postgres_${server}.sql .

  if [ $to_replace = '--replace-db' ]; then
    rake db:drop
    rake db:create
    scp -rp ${connection}:/srv/clients/cluster-pushchino/application/shared/public/uploads/staging/** public/uploads/development
    psql -d $localdb -f postgres_${server}.sql
    rake db:migrate
  fi
fi
