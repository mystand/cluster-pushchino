/*! Pushy - v0.9.2 - 2014-9-13
* Pushy is a responsive off-canvas navigation menu using CSS transforms & transitions.
* https://github.com/christophery/pushy/
* by Christopher Yee */
var pushyInit = function(){
  var pushy = $('.pushy'), //menu css class
    body = $('body'),
    container = $('#container'), //container css class
    push = $('.push'), //css class to add pushy capability
    siteOverlay = $('.site-overlay'), //site overlay
    pushyClass = "pushy-left pushy-open", //menu position & menu open class
    pushyActiveClass = "pushy-active", //css class to toggle site overlay
    containerClass = "container-push", //container open class
    pushClass = "push-push", //css class to add pushy capability
    menuBtn = $('.menu-btn, .pushy-link'), //css classes to toggle the menu
    menuSpeed = 200, //jQuery fallback menu speed
    menuWidth = pushy.width() + "px"; //jQuery fallback menu width
    submenuClass = '.pushy-submenu',
    submenuOpenClass = 'pushy-submenu-open',
    submenuClosedClass = 'pushy-submenu-closed',
    submenu = $(submenuClass);

  function togglePushy(){
    body.toggleClass(pushyActiveClass); //toggle site overlay
    pushy.toggleClass(pushyClass);
    container.toggleClass(containerClass);
    push.toggleClass(pushClass); //css class to add pushy capability
  }

  function openPushyFallback(){
    body.addClass(pushyActiveClass);
    pushy.animate({left: "0px"}, menuSpeed);
    container.animate({left: menuWidth}, menuSpeed);
    push.animate({left: menuWidth}, menuSpeed); //css class to add pushy capability
  }

  function closePushyFallback(){
    body.removeClass(pushyActiveClass);
    pushy.animate({left: "-" + menuWidth}, menuSpeed);
    container.animate({left: "0px"}, menuSpeed);
    push.animate({left: "0px"}, menuSpeed); //css class to add pushy capability
  }

  function toggleSubmenu(){
    //hide submenu by default
    $(submenuClass).addClass(submenuClosedClass);
    $(submenuClass).on('click', function(){
      var selected = $(this);
      if( selected.hasClass(submenuClosedClass) ) {
        //hide opened submenus
        $(submenuClass).addClass(submenuClosedClass).removeClass(submenuOpenClass);
        //show submenu
        selected.removeClass(submenuClosedClass).addClass(submenuOpenClass);
      }else{
          //hide submenu
          selected.addClass(submenuClosedClass).removeClass(submenuOpenClass);
      }
    });
  }
  
  function toggleSubmenuFallback(){
    //hide submenu by default
    $(submenuClass).addClass(submenuClosedClass);
    submenu.children('a').on('click', function(event){
      event.preventDefault();
      $(this).toggleClass(submenuOpenClass)
        .next('.pushy-submenu ul').slideToggle(200)
        .end().parent(submenuClass)
        .siblings(submenuClass).children('a')
        .removeClass(submenuOpenClass)
        .next('.pushy-submenu ul').slideUp(200);
    });
  }

  //checks if 3d transforms are supported removing the modernizr dependency
  cssTransforms3d = (function csstransforms3d(){
    var el = document.createElement('p'),
    supported = false,
    transforms = {
        'webkitTransform':'-webkit-transform',
        'OTransform':'-o-transform',
        'msTransform':'-ms-transform',
        'MozTransform':'-moz-transform',
        'transform':'transform'
    };

    // Add it to the body to get the computed style
    document.body.insertBefore(el, null);

    for(var t in transforms){
        if( el.style[t] !== undefined ){
            el.style[t] = 'translate3d(1px,1px,1px)';
            supported = window.getComputedStyle(el).getPropertyValue(transforms[t]);
        }
    }

    document.body.removeChild(el);

    return (supported !== undefined && supported.length > 0 && supported !== "none");
  })();

  if(cssTransforms3d){
    //toggle submenu
    toggleSubmenu();
    //toggle menu
    menuBtn.click(function() {
      togglePushy();
    });
    //close menu when clicking site overlay
    siteOverlay.click(function(){ 
      togglePushy();
    });
  }else{
    //jQuery fallback
    pushy.css({left: "-" + menuWidth}); //hide menu by default
    container.css({"overflow-x": "hidden"}); //fixes IE scrollbar issue

    //keep track of menu state (open/close)
    var state = true;

    //toggle submenu
    toggleSubmenuFallback();

    //toggle menu
    menuBtn.click(function() {
      if (state) {
        openPushyFallback();
        state = false;
      } else {
        closePushyFallback();
        state = true;
      }
    });

    //close menu when clicking site overlay
    siteOverlay.click(function(){ 
      if (state) {
        openPushyFallback();
        state = false;
      } else {
        closePushyFallback();
        state = true;
      }
    });
  }
};

$(document).bind("ready", pushyInit);