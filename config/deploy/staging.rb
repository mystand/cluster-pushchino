set :domain, 'new.mystand.ru'
set :deploy_to, '/srv/clients/cluster-pushchino/application'
set :user, 'cluster-pushchino' # Username in the server to SSH to.

# Optional settings:
#   set :port, '30000'           # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.