Rails.application.routes.draw do

  get 'my_organizations/index'

  get 'my_organizations_controller/index'

  get "/" => redirect("/ru/")
  
  scope '(:locale)', locale: /#{I18n.available_locales.join('|')}/ do

    root to: 'home#index'
    get :greeting, to: 'home#greeting'
    resources :contacts, only: [:index, :create, :destroy]
    resources :posts, only: [:index, :show]
    resources :interviews, only: [:index, :show]
    resources :pages, only: [:index, :show]
    resources :menu_items, only: [:show]
    resources :votes, only: :index do
      post :vote
    end
    resources :user_options, only: :create
    resources :organizations, only: [:index, :edit, :show, :update]
    resources :my_posts, only: [:index, :new, :create]
    resources :my_organizations, only: :index

    namespace :admin do
      root 'dashboard#index'

      resources :users, except: :show
      resources :organizations, except: :show
      resources :pages, except: :show
      # resources :presentations, except: :show
      resources :posts, except: :show
      resources :interviews, except: :show
      resources :activity_types, except: :show
      resources :page_fragments, only: [:index, :edit, :update]
      resources :votes, except: :show
      resources :groups, except: :show
      resources :slides, except: :show
      resources :mailings, except: :show
      resources :contacts, only: [:index, :show]
      resource :settings, except: [:new, :create, :destroy]

      resources :menu_items, except: [:show] do
        member { put :move }
      end

      resources :imperavi_pictures, only: [:index, :create, :show, :destroy]
      resources :imperavi_attachments, only: [:index, :create, :destroy]
    end

    resources :messages, only: [:edit, :create, :update, :destroy]

    resources :topics, path: '/forum' do
      member do
        post :archive
        post :unarchive
      end
      collection { get :archived }
    end

    devise_for(:users,
      controllers: {
        sessions: 'sessions',
        registrations: 'registrations',
        passwords: 'passwords'
      }
    )

    devise_scope :user do
      get 'register', to: 'registrations#new'
      get 'profile', to: 'registrations#edit'
      get 'login', to: 'sessions#new'
    end
  end

  match '/404' => 'errors#error404', via: [ :get, :post, :put, :patch, :delete ]
end
