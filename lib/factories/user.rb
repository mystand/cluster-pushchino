FactoryGirl.define do
  factory :user do
    first_name {Faker::Name.first_name}
    last_name  {Faker::Name.last_name}
    middle_name {Faker::Name.prefix}
    password "123qwe123qwe"
    password_confirmation "123qwe123qwe"
    role 'normal'
    post {Faker::Name.title}
    email {Faker::Internet.email}
    organization
  end
end

Dir[File.join(Rails.root, "lib", "factories", "*")].each{|s| require s}

