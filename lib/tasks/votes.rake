namespace :votes do
  task :notify => :environment do
    votes = Vote.where(closed: false).where('date_end <= ?', Time.now)
    admins = User.where id: (Settings.first.vote_notification_user_ids || [])
    votes.each do |vote|
      admins.each do |admin|
        UserMailer.delay.vote_ready(admin, vote)
      end
    end
  end
end