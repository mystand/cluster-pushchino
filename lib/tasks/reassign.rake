namespace :reassign do
  desc "reassign organizations to users"
  task start: :environment do
    User.all.each do |user|
      if (user.organization_id)
        user.organizations.push Organization.find(user.organization_id)
      end
    end
  end
end