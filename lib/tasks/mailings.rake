namespace :mailings do
  task :send => :environment do
    mailings_to_send = Mailing.where status: Mailing::STATUSES[:waiting]
    mailings_to_send.each do |mailing|
      mailing.users.each do |user|
        MailingJob.perform_now(mailing, user)
      end  
    end
  end
end