module Responders
  module AdminCollectionResponder
    protected

    def navigation_location
      namespace = options[:prefixes].first.split('/')
      namespace.pop
      options[:location] = [:edit, *namespace, resource] if controller.params[:continue]
      return options[:location] if options[:location]
      klass = resources.last.class

      if klass.respond_to?(:model_name)
        resources[0...-1] << klass.model_name.route_key.to_sym
      else
        resources
      end
    end
  end
end