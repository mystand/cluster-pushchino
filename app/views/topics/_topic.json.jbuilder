json.extract! topic, :id, :title, :is_archieved, :created_at, :updated_at
json.url topic_url(topic, format: :json)