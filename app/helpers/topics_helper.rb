module TopicsHelper
  def topic_type_icon(topic_type)
    res = case topic_type
      when 'open'
        content_tag :div, class: "icon-wrapper icon-#{topic_type}", title: t("helpers.topics.type.#{topic_type}") do
          content_tag :i, '', class: 'fa fa-icon fa-eye'
        end
      when 'private'
        content_tag :div, class: "icon-wrapper icon-#{topic_type}", title: t("helpers.topics.type.#{topic_type}") do
          content_tag :i, '', class: 'fa fa-icon fa-eye-slash'
        end
      else ''
    end
  end
end
