module ApplicationHelper
  def flash_messages
    return unless flash.any?

    items = []
    flash.each do |name, msg|
      items << content_tag(:li, raw(msg), id: "flash-#{name}")
    end

    content_tag :div, class: "flash-messages #{flash.keys.join(' ')}" do
      concat(content_tag :ul, raw(items.join))
      concat((link_to('#', class: 'close-link')  do
          concat(content_tag :i, '', class: 'fa fa-times')
        end)
      )
    end
  end

  def menu_item_smart_url obj
    # url = if obj.target
    #     obj.target.try(:url) || url_for(obj.target)
    #   else
    #     obj.url
    #   end
    # url = url_for(obj) if url.blank?
    # url
    
    url = if obj.target
        if obj.try(:column_names).try(:include?, :semantic_url).present?
          obj.target.try(:url) || url_for(obj.target)
        else
          url_for(obj.target)
        end
      else
        obj.url
      end
    url = url_for(obj) if url.blank?
    url
  end

  def locales_selector
    content_tag :div, class: "locale-selector" do
      content_tag :div, class: "wrapper" do
        current = link_to '#', class: "locale locale-#{I18n.locale}" do
          [content_tag(:div, "", class: "flag"),
          content_tag(:span, t("locale_selector.#{I18n.locale}")),
          content_tag(:div, "", class: "arrow")].join.html_safe
        end
        others = I18n.available_locales.map do |locale|
          unless locale == I18n.locale
            link_to url_for(locale: locale), class: "locale locale-#{locale} inactive" do
              [content_tag(:div, "", class: "flag"),
              content_tag(:span, t("locale_selector.#{locale}")),
              content_tag(:div, "", class: "arrow")].join.html_safe
            end
          end
        end
        [current, others].join("").html_safe
      end
    end
  end

  def font_size_selector
    content_tag :div, class: 'font-size-selector' do
      available_sizes = ['small', 'medium', 'big']

      mobile = content_tag :div, class: 'wrapper mobile' do
        selected = available_sizes.include?(@font_size) ? @font_size : 'small'
        current = link_to 'Aa', '#', class: "item #{selected} active"
        others = available_sizes.map do |key|
          url = "?font_size=#{key}"
          link_to 'Aa', url, class: "item #{key}"
        end
        [current, others].join('').html_safe
      end

      desktop = content_tag :div, class: 'wrapper desktop' do
        available_sizes.map do |key|
          url = "?font_size=#{key}"
          selected = available_sizes.include?(@font_size) ? @font_size : 'small'
          link_to 'Aa', url, class: "item #{key} #{selected == key ? 'active' : ''}"
        end.join('').html_safe
      end

      [mobile, desktop].join('').html_safe
    end
  end

  def render_frontend_breadcrumbs
    return unless @breadcrumbs.try(:any?)
    content_tag :ol, class: 'breadcrumbs-front' do
      @breadcrumbs.map do |obj|
        if obj[:url]
          content_tag :li, link_to(obj[:name], obj[:url])
        else
          content_tag :li, class: 'active' do
            content_tag :span, obj[:name]
          end
        end
      end.join.html_safe
    end
  end

  def popup_text(resource)
    ["<h4>#{resource.name}</h4>", resource.address].reject{|c| c.empty? }.join('<br/>')
  end
end
