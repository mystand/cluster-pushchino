module Admin::MailingsHelper

  def draw_mailing_status(mailing)
      status = mailing.status
      klasses = {
        new: "label-default",
        waiting: "label-warning",
        sending: "label-primary",
        done: "label-success"
      }
      
      content_tag :span, class: "label #{klasses[mailing.status.to_sym]}" do
        I18n.t("helpers.mailing_statuses.#{mailing.status}")
      end
  end
end
