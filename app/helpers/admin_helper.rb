module AdminHelper
  def submit_and_continue(f)
    f.button :submit, name: :continue, value: "Сохранить и продолжить"
  end

  def published_status(resource)
    if resource.published?
      fa_icon "check", class: "text-primary"
    else
      fa_icon "close", class: "text-muted"
    end
  end

  def render_breadcrumbs
    return unless @breadcrumbs.try(:any?)
    content_tag :ol, class: 'breadcrumb' do
      @breadcrumbs.map do |obj|
        if obj[:url]
          content_tag :li, link_to(obj[:name], obj[:url])
        else
          content_tag :li, class: 'active' do
            content_tag :strong, obj[:name]
          end
        end
      end.join.html_safe
    end
  end

  def error_explanation(target, options = {})
    options[:filter] ||= []
    if target.errors.any?
      content_tag :div, class: "row" do
        content_tag :div, class: "col-lg-10 col-lg-offset-#{options[:offset]}" do
          content_tag :ul, class: "error-messages alert alert-danger  " do
            target.errors.messages.each do |key, _|
              unless options[:filter].include?(key)
                target.errors.full_messages_for(key).each do |msg|
                  concat content_tag :li, msg
                end
              end
            end
          end
        end
      end
    end
  end

  def bool_to_word(bool, type = :word)
    !bool.nil? ? I18n.t("helpers.bool_to.#{type}.if_#{bool}") : ''
  end
  
  def locale_pickers
    [locales_picker(:left, :ru, class: 'col-lg-6 col-md-12 col-sm-12 col-xs-12'), locales_picker(:right, :en,  class: 'col-lg-6 col-md-12 col-sm-12 col-xs-12')].join.html_safe
  end

  def locales_picker(type, selected, options = {})
    locales_arr = []
    locales_arr.push [I18n.t('helpers.locales.empty'), nil] if options[:include_blank]
    I18n.available_locales.map do |locale|
      locales_arr.push [I18n.t("helpers.locales.#{locale}"), locale]
    end
    content_tag :div, class: options[:class] do
      concat select_tag type, options_for_select(locales_arr, selected: selected), class: "locale-selector #{type}", data: { locale_type: type }
    end
  end
end