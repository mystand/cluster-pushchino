class UserMailer < ApplicationMailer
  def approval_request(user)
    @user = user
    mail(to: "admin@cluster-pushchino.change.me", subject: 'Запрос подтверждения учетной записи')
  end

  def registration_info(user)
    @user = user
    mail(to: @user.email, subject: 'Вы успешно зарегистрировались')
  end

  def account_approved(user)
    @user = user
    mail(to: @user.email, subject: 'Ваша учетная запись подтверждена')
  end

  def notify_admin_on_registration_info(admin, user)
    @admin = admin
    @user = user
    mail(to: @admin.email, subject: "Зарегистрирован новый пoльзователь: #{@user.email}")
  end

  def vote_ready(admin, vote)
    @admin = admin
    @vote = vote
    mail(to: @admin.email, subject: 'Окончен срок голосования')
  end

  def notify_admin_on_feedback(admin, contact)
    @admin = admin
    @contact = contact
    mail(to: @admin.email, subject: 'Был получен новый отзыв')
  end

end