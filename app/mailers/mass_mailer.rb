class MassMailer < ApplicationMailer
  def start(user, mailing)
    @mailing = mailing
    @mailing.mailing_attachments.each do |attachment|
      attachments[attachment.file.name] = attachment.file.data
    end

    mail(to: user.email, subject: @mailing.title)
  end
end
