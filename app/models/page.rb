class Page < ActiveRecord::Base

  validates :title, presence: true

  multilang :title, :intro, :content

  has_one :menu_item, as: :target, dependent: :nullify

  acts_as_url :title, limit: 64, truncate_words: false

  def as_indexed_json(_ = {})
    as_json(only: [:title, :intro, :content])
  end

  def self.find_by_url_or_id! str
    res = self.where("pages.url = ? or pages.id = ?", str.to_s, str.to_i).first
    raise ActiveRecord::RecordNotFound unless res
    res
  end

  def name
    title
  end

  def to_s
    name
  end

  def siblings(options = {})
    return [] unless menu_item
    items = menu_item.parent.children.select do |item|
      item.target_type == 'Page' && item.target != self
    end
    items.select!(&:published) if options[:published]
    items.map(&:target)
  end

  def to_param
    url
  end
end
