class Slide < ActiveRecord::Base
  include ImageUploading

  multilang :title, :subtitle

  validates :title, :image, presence: true

  scope :published, -> { where(published: true) }
  scope :desc, -> { order(created_at: :desc) }
end
