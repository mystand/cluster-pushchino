module HasPictures
  extend ActiveSupport::Concern

  included do
    has_many :pictures, as: :target, dependent: :destroy
    accepts_nested_attributes_for :pictures, 
      allow_destroy: true,
      reject_if: lambda { |p| p[:file].blank? }

    def export
      res = attributes.merge(pictures: pictures.map(&:export))
      res
    end
  end
end