class User < ActiveRecord::Base
  include ActiveModel::Dirty
  # include ImageUploading
  dragonfly_accessor :image do
    default 'public/images/profile.png'
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable

  ROLES = [:admin, :moderator, :normal, :guest]

  attr_accessor :organization_not_in_list

  validates :role, inclusion: ROLES.map(&:to_s)
  validates :first_name, :middle_name, :last_name, :post, presence: true
  validates :organizations, presence: true, if: Proc.new{
    |u| (!u.organization_not_in_list? && u.outer_organization.blank? && !role?(:admin)) 
  }
  validates :outer_organization, presence: true, if: Proc.new{ |u| (u.organization_not_in_list? && !role?(:admin)) }

  before_validation :setup_role
  after_create :send_approval_request, 
               :send_registration_info,
               if: Proc.new { |user| user.role?(:normal) }
  after_update :check_approval,
               if: Proc.new { |user| user.role?(:normal) } 

  after_commit :notify_admin_on_registration_info, on: [:create],
               if: Proc.new { |user| user.role?(:normal) }

  has_and_belongs_to_many :organizations

  has_many :group_users, dependent: :destroy
  has_many :groups, through: :group_users

  has_many :user_options
  scope :active, -> { where(approved: true) }

  # alias_method :img, :image
  # def image
  #   img || Dragonfly.app.fetch_file(File.join(Rails.root, 'app', 'assets', 'images', 'profile.png'))
  # end

  def setup_role
    unless role.present?
      if organization_not_in_list? && outer_organization.present?
        self.role = :guest
      elsif organization.present?
        self.role = :normal
      end
    end
  end

  def role?(role)
    return false unless ROLES.include?(role.to_sym)
    role.to_sym == self.role.try(:to_sym)
  end

  def active_for_authentication? 
    super && (role?(:guest) && approved?) ||  
    (role?(:normal) && approved?) || role?(:admin) || role?(:moderator)
  end 

  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end

  def send_approval_request
    UserMailer.approval_request(self).deliver_now
  end

  def send_registration_info
    UserMailer.registration_info(self).deliver_now
  end

  def notify_admin_on_registration_info
    admins = User.where id: (Settings.first.user_notification_user_ids || [])
    admins.each do |admin|
      UserMailer.delay.notify_admin_on_registration_info(admin, self)
    end
  end

  def check_approval
    if approved_changed?
      UserMailer.account_approved(self).deliver_now
    end
  end

  def full_name
    name = [last_name, first_name, middle_name].join(" ")
    unless name.blank?
      name
    else
      email
    end
  end

  def short_name
    m = "#{middle_name[0]}." if middle_name
    name = [first_name, m, last_name].compact.join(" ")
  end

  def to_s
    full_name
  end

  # def admin
  #   role?(:admin)
  # end

  # def manager
  #   role?(:manager)
  # end

  def organization_not_in_list?
    organization_not_in_list.to_i.zero? ? false : true
  end

  def voted?(vote)
    organization_ids = self.organizations.map {|organization| organization.id}
    voted_ids = self.voted_options(vote).map {|option| option.organization_id}
    organization_ids & voted_ids == organization_ids
  end

  def voted_options(vote)
    UserOption.where(organization_id: self.organizations.map {|organization| organization.id}, vote_id: vote.id)
  end

  def organization_options
    organizations = []
    self.organizations.each do |organization|
      organizations.push({
        :id => organization.id,
        :name => organization.name
      })
    end
    organizations
  end


end
