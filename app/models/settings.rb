class Settings < ActiveRecord::Base

  dragonfly_accessor :greeting_image
  multilang :site_name, :greeting_title, :greeting_short_description, :greeting_paragraph_1, :greeting_paragraph_2

end
