class Topic < ActiveRecord::Base

  attr_accessor :text
  attr_accessor :group_ids

  TOPIC_OPEN = 'open'
  TOPIC_PRIVATE = 'private'
  TOPIC_TYPES = [TOPIC_OPEN, TOPIC_PRIVATE]

  belongs_to :user
  has_many :messages, autosave: true
  has_and_belongs_to_many :users

  validates :title, :user, :text, presence: true
  validates :topic_type, inclusion: TOPIC_TYPES

  before_save :pluck_users

  after_initialize :build_first_message, if: :new_record?
  after_save :change_first_message, on: :update

  accepts_nested_attributes_for :messages
  accepts_nested_attributes_for :users

  scope :by_user, -> (id) { where("topic_type='open' OR (topic_type='private' AND user_id = ?)", id) }
  scope :archived, -> { where is_archieved: true }
  scope :not_archived, -> { where is_archieved: false }
  scope :by_last_message_at, -> { order last_message_at: :desc }

  def initialize(args={})
    super
    topic_type ||= TOPIC_PRIVATE
  end

  def text
    @text || self.messages.first.try(:text)
  end

  def archive!
    self.update is_archieved: true
  end

  def unarchive!
    self.update is_archieved: false
  end

  private

  def build_first_message
    self.messages.build topic: self, user: self.user, text: self.text
  end

  def change_first_message
    self.messages.first.update text: self.text
  end

  def pluck_users
    return true if self.group_ids.nil?
    arr = self.user_ids
    group_ids = self.group_ids.delete_if{|i| i.blank? }
    Group.find(group_ids).each do |group|
      arr += group.users.active.ids
    end
    self.user_ids = arr.uniq
    self.group_ids = nil
    true
  end

end
