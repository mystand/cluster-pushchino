class PageFragment < ActiveRecord::Base
  multilang :content

  validates :name, :key, presence: true

  scope :key, -> (key) { where(key: key) }
end
