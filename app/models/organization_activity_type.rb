class OrganizationActivityType < ActiveRecord::Base
  belongs_to :activity_type
  belongs_to :organization
end
