class Ability
  include CanCan::Ability

  def initialize(user)
    cannot :read, Vote unless user
    can :read, Organization

    user ||= User.new
    can [:read, :update], User, id: user.id if user.id.present?
    can [:read], Post
    can [:read], Page
    can [:read], MenuItem
    can [:read], Organization
    can [:read], Interview

    case user.role.try(:to_sym)
      when :admin
        can :manage, :all
      when :moderator
        can :manage, [Vote, VoteGroup, GroupUser, UserOption, VoteUser]
        can :manage, [MenuItem, Page]
        can :manage, PageFragment
        can :manage, Interview
        can :manage, Contact
        can :manage, Post
        can :manage, [Presentation, PresentationSlide, Slide]
        can :manage, Settings
        can :manage, [Topic, Message]
      when :normal
        can :manage, Organization, users: { id: user.id }
        can [:read, :vote], Vote, Vote.joins("left join vote_users on vote_users.vote_id = votes.id ").
                              where("(vote_users.id is null) or (vote_users.user_id = ?)", user.id).
                              select("DISTINCT ON (votes.id) votes.*").order('votes.id') do |vote|
          vote.vote_users.empty? || vote.vote_users.map(&:user_id).include?(user.id)
        end

        can :read, Topic, Topic.joins("left join topics_users on topics_users.topic_id = topics.id").
          where("(topics.topic_type = ?) or " + 
          "((topics.topic_type = ?) and " + 
          " (topics_users.user_id = ?)) or " + 
          "(topics.user_id = ?)" , Topic::TOPIC_OPEN, Topic::TOPIC_PRIVATE,  user.id,  user.id).uniq do |topic|
            topic.user_id == user.id ||
            topic.topic_type == Topic::TOPIC_OPEN || 
            (topic.topic_type == Topic::TOPIC_PRIVATE &&
            topic.user_ids.include?(user.id)) 
          end  
        can :create, Topic do |topic|
          topic.topic_type == Topic::TOPIC_PRIVATE
        end  
        can [:update, :destroy], Topic, user_id: user.id

        can [:update, :destroy], Message, user_id: user.id
        can :create, Message do |message|
          topic = message.topic
          topic.user_id == user.id ||
            topic.topic_type == Topic::TOPIC_OPEN || 
            (topic.topic_type == Topic::TOPIC_PRIVATE &&
            topic.user_ids.include?(user.id)) 
        end

        can :manage, Post, user_id: user.id

      when :guest
        can :read, Vote, Vote.joins("left join vote_users on vote_users.vote_id = votes.id ").
                              where("(vote_users.id is null) or (vote_users.user_id = ?)", user.id).
                              select("DISTINCT ON (votes.id) votes.*").order('votes.id') do |vote|
          vote.vote_users.empty? || vote.vote_users.map(&:user_id).include?(user.id)
      
        end

        can :read, Topic, Topic.joins("left join topics_users on topics_users.topic_id = topics.id").
          where("(topics.topic_type = ?) or " + 
          "((topics.topic_type = ?) and " + 
          " (topics_users.user_id = ?)) or " + 
          "(topics.user_id = ?)" , Topic::TOPIC_OPEN, Topic::TOPIC_PRIVATE,  user.id,  user.id).uniq do |topic|
            topic.user_id == user.id ||
            topic.topic_type == Topic::TOPIC_OPEN || 
            (topic.topic_type == Topic::TOPIC_PRIVATE &&
            topic.user_ids.include?(user.id)) 
          end  
        can :create, Topic do |topic|
          topic.topic_type == Topic::TOPIC_PRIVATE
        end  
        can [:update, :destroy], Topic, user_id: user.id

        can [:update, :destroy], Message, user_id: user.id
        can :create, Message do |message|
          topic = message.topic
          topic.user_id == user.id ||
            topic.topic_type == Topic::TOPIC_OPEN || 
            (topic.topic_type == Topic::TOPIC_PRIVATE &&
            topic.user_ids.include?(user.id)) 
        end

        can :manage, Post, user_id: user.id
    end
  end
end
