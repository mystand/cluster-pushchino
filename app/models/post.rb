class Post < ActiveRecord::Base
  include ImageUploading
  include HasPictures

  acts_as_url :title, limit: 64, truncate_words: false

  belongs_to :user
  multilang :title, :intro, :content

  validates :title, :image, presence: true

  scope :published, -> { where published: true }
  scope :desc, -> { order(publish_date: :desc) }
  scope :except_id, -> (id) { where.not(id: id) }
  scope :by_year, -> (y) { where('extract(year from publish_date) = ?', y) }
  scope :by_month, -> (m) { where('extract(month from publish_date) = ?', m) }
  scope :by_user_id, -> (id) { where(user_id: id) }

  def to_s
    title
  end

  def to_param
    url
  end
end
