class Group < ActiveRecord::Base
  validates :title, presence: true

  has_many :group_users, dependent: :destroy
  has_many :users, through: :group_users

  has_many :vote_groups, dependent: :destroy
  has_many :votes, through: :vote_groups

  def to_s
    title
  end
end
