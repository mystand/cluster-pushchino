class Presentation < ActiveRecord::Base
  has_many :slides, class_name: "PresentationSlide", foreign_key: :presentation_id, dependent: :destroy
  has_one :menu_item, as: :target, dependent: :nullify

  validates :name, presence: true

  scope :by_published, -> { where published: true }

  multilang :name

  accepts_nested_attributes_for :slides, allow_destroy: true

  def title
    name
  end

  def intro
    'intro'
  end
end
