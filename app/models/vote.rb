class Vote < ActiveRecord::Base
  multilang :title, :description

  validates :title, :date_start, :date_end, presence: true
  validates :options, presence: true

  has_many :options, dependent: :destroy

  has_many :user_options

  has_many :organizations, -> { distinct }, through: :user_options

  accepts_nested_attributes_for :options, allow_destroy: true

  has_many :vote_groups, dependent: :destroy
  has_many :groups, through: :vote_groups

  has_many :vote_users, dependent: :destroy
  has_many :users, through: :vote_users
  
  before_save :pluck_users
  before_save :update_organizations_count 
  scope :published, -> { where published: true }

  def to_s
    title
  end

  private

  def update_organizations_count
    count = 0
    if self.results
      self.options.each do |obj|
        count += self.results[obj.id.to_s].to_i
      end
    end
    self.organizations_count = count
  end

  def pluck_users
    arr = self.user_ids
    self.groups.each do |group|
      arr += group.user_ids
    end
    self.user_ids = arr.uniq
    self.group_ids = []
  end

  

end