class VoteUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :vote

  validates :user, :vote, presence: true
end
