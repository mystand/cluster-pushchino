class Mailing < ActiveRecord::Base
  STATUSES = {new: 'new', waiting: 'waiting', sending: 'sending', done: 'done'}

  has_and_belongs_to_many :users
  has_many :mailing_attachments
  before_save :update_sending_data
  accepts_nested_attributes_for :mailing_attachments, allow_destroy: true
  attr_accessor :group_ids
  
  validates :title, :body, presence: true
  
  public
  
  def user_ids= ids
    group_user_ids = Group.where(id:@group_ids).map(&:user_ids).flatten.compact.uniq
    ids += group_user_ids
    super ids
  end

  def group_ids; []; end

  def done_with! user
    self.sending_data.delete(user.id)
    if self.sending_data.empty?
      self.status = STATUSES[:done]
    end
    self.save
  end

  def repeat!
    res = Mailing.new title: self.title, body: self.body, users: self.users 
    self.mailing_attachments.each do |attachment|
      res.mailing_attachments.build file: attachment.file 
    end
    res.save!   
    res
  end
  
  private

  def update_sending_data
    if self.status == STATUSES[:new]
      self.sending_data = {}
      self.users.each do |user|
        self.sending_data[user.id]
      end 
    end    
  end

end
