class OrganizationAttachment < ActiveRecord::Base
  belongs_to :organization
  validates :file, presence: true

  dragonfly_accessor :file
end