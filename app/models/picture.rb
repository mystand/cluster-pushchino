class Picture < ActiveRecord::Base
  belongs_to :target, polymorphic: true

  validates :file, presence: true

  dragonfly_accessor :file
end
