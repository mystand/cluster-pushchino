class ActivityType < ActiveRecord::Base
  has_many :organization_activity_types, dependent: :destroy
  has_many :organizations, through: :organization_activity_types

  multilang :name

  validates :name, presence: true

  def to_s
    name
  end
end
