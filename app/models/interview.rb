class Interview < ActiveRecord::Base
  include ImageUploading
  include HasPictures

  multilang :title, :intro, :content

  validates :title, :image, presence: true

  scope :published, -> { where published: true }
  scope :desc, -> { order(publish_date: :desc) }

  acts_as_url :title, limit: 64, truncate_words: false

  def to_s
    title
  end

  def to_param
    url
  end
end
