class PresentationSlide < ActiveRecord::Base
  include ImageUploading

  belongs_to :presentation

  validates :image, presence: true
  validates :title, :text, :order_number, presence: true

  scope :by_order_number, -> { order :order_number }

  multilang :title, :text

end
