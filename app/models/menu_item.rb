class MenuItem < ActiveRecord::Base
  extend Dragonfly::Model
  extend Dragonfly::Model::Validations

  # needed for the dependence
  has_many :unordered_children, class_name: 'MenuItem',
                                foreign_key: 'parent_id',
                                dependent: :restrict_with_error

  belongs_to :parent, class_name: 'MenuItem'
  belongs_to :target, polymorphic: true

  multilang :title, :annotation, :responsible_person

  # validate :annotation_length

  validates :title, presence: true
  validates :parent_id, presence: true, unless: :root
  validates :key, uniqueness: true, allow_blank: true, allow_nil: true
  validate :not_a_cycle

  after_create :create_target_resource
  before_destroy :destroy_related_page

  dragonfly_accessor :icon

  scope :published, -> { where(published: true) }

  acts_as_url :title, limit: 64, truncate_words: false, url_attribute: :semantic_url

  def name
    title
  end

  def to_param
    semantic_url
  end

  def to_s; name; end

  def self.root
    MenuItem.where(root: true).first
  end

  def self.get(key)
    MenuItem.where(key: key).first
  end

  def children(*p)
    unless p.length.zero?
      children_ids.map do |id|
        query_params = { id: id }
        query_params.merge!(p[0])
        MenuItem.where(query_params).first
      end.compact
    else
      children_ids.map do |id|
        MenuItem.find_by_id id
      end.compact
    end
  end

  def remove_child_id!(child_id)
    child_id = child_id.to_s
    index    = children_ids.index child_id

    fail "Leaf #{child_id} not found in #{id}" if index.nil?

    new_children_ids = children_ids - [child_id]
    update_attributes! children_ids: new_children_ids

    index
  end

  def add_child_id!(child_id, new_index = children_ids.count)
    child_id  = child_id.to_s
    # old_index = children_ids.index child_id

    # raise "Leaf #{child_id} already in #{id}" unless old_index.nil?

    new_children_ids = children_ids - [child_id]
    new_children_ids.insert new_index, child_id
    update_attributes! children_ids: new_children_ids

    new_index
  end

  def image
    target ? target.image : orignal_image
  end

  alias_method :original_title, :title

  def title
    target ? target.title : original_title
  end

  alias_method :original_annotation, :annotation

  def annotation
    target ? target.intro : original_annotation
  end

  def child_id_index(child_id)
    child_id = child_id.to_s
    children_ids.index child_id
  end

  def parent_list(options = {})
    p = parent
    list = []
    while p
      list << p if p.breadcrumbs
      p = p.parent
    end
    list.pop if options[:no_root]
    list
  end

  def as_json(_)
    res = super
    res.merge!(target_name: target.title) if target
    res
  end

  def as_indexed_json(_ = {})
    as_json(only: [:title, :annotation, :responsible_person])
  end

  private

  def not_a_cycle
    parent_list.each do |p|
      errors.add(:parent_id, :cycle) if p == self
    end
  end

  def annotation_length
    # используется метод из-за двух причин:
    # 1 - валидация с помощью multilang-hstore ужасна и не годна
    # 2 - в случае I18n.locale.each{|i| validates ... "annotation_#{i}" ... }
    # error кидается не на annotation, а на annotation_#{locale}
    I18n.available_locales.each do |locale|
      annotation = send "annotation_#{locale}"
      if annotation.present?
        errors.add :annotation, I18n.t('errors.messages.too_long.few', count: 500) if annotation.length > 500
        errors.add :annotation, I18n.t('errors.messages.too_short.few', count: 150) if annotation.length < 150
      else
        errors.add :annotation, :blank
      end
    end
  end

  def create_target_resource
    if target_type == 'Page'
      page = Page.create!(title: self.title)
      update_attributes! target_id: page.id if page
    elsif target_type == 'Presentation'
      p = Presentation.create!(name: 'Новая презентация')
      update_attributes! target_id: p.id if p
    else
      return
    end
  end

  def destroy_related_page
    # return unless target_id && target_type == 'Page'
    if target_id && target_type == 'Page'
      Page.find(target_id).destroy
    elsif target_id && target_type == 'Presentation'
      Presentation.find(target_id).destroy
    else
      return
    end
  end

end
