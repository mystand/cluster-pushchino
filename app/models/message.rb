class Message < ActiveRecord::Base
  include ActionView::Helpers

  default_scope { order('created_at asc') } 

  after_create :update_topic_last_message_at

  belongs_to :user # author
  belongs_to :editor, class_name: 'User'
  belongs_to :topic

  validates :user, :topic, presence: true

  validate :sanitized_trimmed_text_isnt_empty

  private

  def sanitized_trimmed_text_isnt_empty
    if text.nil? || sanitize(text, tags: ['img']).strip.empty?
      errors.add(:text, t('errors.messages.blank'))
    end
  end

  def update_topic_last_message_at
    topic.update last_message_at: created_at
  end

end
