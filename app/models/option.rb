class Option < ActiveRecord::Base
  multilang :title, :description

  validates :title, presence: true

  belongs_to :vote
  has_many :user_options
end
