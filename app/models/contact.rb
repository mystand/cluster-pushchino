class Contact < ActiveRecord::Base
  validates :email, :name, :subject, :body, presence: true

  after_commit :notify_admin_on_feedback, on: [:create]

  def notify_admin_on_feedback
    admins = User.where id: (Settings.first.feedback_notification_user_ids || [])
    admins.each do |admin|
      UserMailer.delay.notify_admin_on_feedback(admin, self)
    end
  end
end