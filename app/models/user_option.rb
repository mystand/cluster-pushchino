class UserOption < ActiveRecord::Base
  belongs_to :user
  belongs_to :option
  belongs_to :organization
  belongs_to :vote
  validates :user, :option, :organization_id, presence: true
  # validates :organization_id, uniqueness: {scope: [:vote_id]} TODO problems with allow_multiple_answer
  validate :vote_includes_option
  after_create :update_vote_cache

  private

  def update_vote_cache
    vote.results = {} unless vote.results
    id = option.id.to_s
    res = vote.results[id]
    vote.results[id] = res ? res + 1 : 1
    vote.save
  end

  def vote_includes_option
    errors.add(:option_id, "Несуществующий вариант ответа для этого голосования") unless vote.options.include?(option)
  end
end