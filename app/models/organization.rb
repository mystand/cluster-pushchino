class Organization < ActiveRecord::Base
  include HasPictures
  validates :name, presence: true

  has_many :organization_activity_types, dependent: :destroy
  has_many :activity_types, through: :organization_activity_types
  has_and_belongs_to_many :users
  has_many :organization_attachments, dependent: :destroy
  accepts_nested_attributes_for :organization_attachments, :pictures, allow_destroy: true
  accepts_nested_attributes_for :activity_types

  multilang :name, :chief_name, :competence, :capabilities,
            :offers, :interests, :contacts, :address

  dragonfly_accessor :logo

  geojson_accessor :coordinates

  scope :in_slider, -> { where(in_slider: true) }
  scope :by_query, -> query { where("name ->> :key ILIKE :value", key: I18n.locale.to_s, value: "%#{query}%") }

  acts_as_url :name, limit: 64, truncate_words: false

  def self.search search_string
    where("name ->> :key ILIKE :value", key: I18n.locale.to_s, value: "%#{search_string}%")
  end

  def to_param
    url
  end
end
