class MailingAttachment < ActiveRecord::Base
  belongs_to :mailing
  validates :file, presence: true
  dragonfly_accessor :file
end
