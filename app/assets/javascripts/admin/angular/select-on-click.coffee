window.app = angular.module("menuTreeEdit", []) unless window.app

app.directive 'selectOnClick', ->
  restrict: 'A',
  link: (scope, element, attrs) ->
    element.on 'click', -> this.select()
    element.on 'keypress', (ev) ->
      if ev.which == 13
        $("*").blur();
        no

