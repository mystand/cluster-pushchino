window.app = angular.module("menuTreeEdit", []) unless window.app

app.directive 'actsAsDropdown', ->
  restrict: 'A',
  link: (scope, element, attrs) ->
    linkEl = $(".dropdown-toggle", element)
    menu = $(".dropdown-menu", element)
    linkEl.on 'click', ->
      menu.toggle()

    $(window).on 'click', (ev) ->
      if !_.include $(ev.target).parents().andSelf(), linkEl[0]
        menu.hide()