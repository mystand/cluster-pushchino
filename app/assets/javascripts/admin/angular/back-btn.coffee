window.app = angular.module("menuTreeEdit", []) unless window.app

app.directive 'backBtn', ->
  restrict: 'A',
  link: (scope, element, attrs) ->
    element.on 'click', ->
      window.history.back()
      no
