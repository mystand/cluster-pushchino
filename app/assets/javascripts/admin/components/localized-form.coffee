class LocalizedForm
  constructor: (el) ->
    @el = $ el
    @localeSelectors = $ ".locale-selector", @el
    @localeSelectors.change @update
    @setState @readCookie() if @readCookie() 
    
    @update()

    @el.on 'change', '.localized-row input, textarea', @fillBlankLocales

    $(document).on 'nested:fieldAdded', @update 
 
  update: =>
    @localizedFields = $ "[data-locale]", @el
    console.log(@localizedFields)
    state = @getState()
    for field in @localizedFields
      column = state[$(field).data("locale")] || null
      $(field).attr "localize-view", column
    state

  setState: (state) =>
    for key, value of state
      selector = $ "select[data-locale-type='#{key}']", @el
      selector.val value

  swapObj: (obj) =>
    res = {}
    for key, val of obj
      res[val] = key
    res

  getState: =>
    state = {}
    for selector in @localeSelectors
      $selector = $ selector
      state[$selector.val()] = $selector.data "locale-type"
    @writeCookie @swapObj(state)
    state

  readCookie: =>
    cookie = $.cookie "localized-state"
    JSON.parse cookie if cookie 

  writeCookie: (state) =>
    $.cookie "localized-state", JSON.stringify(state) , {path: '/'}


  fillBlankLocales: (ev) =>
    target = $ ev.currentTarget
    val = target.val()
    parent = target.parents(".localized-row")
    for input in parent.find("input, textarea")
      localizedVal = $(input).val()
      $(input).val val if val && !localizedVal

window.addComponent LocalizedForm, className: "localized-form"
