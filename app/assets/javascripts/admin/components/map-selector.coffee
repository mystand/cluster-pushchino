class MapSelector
  constructor: (el)->
    @input = $ el
    @input.wrap "<div class='map-selector-container'>"
    @el = @input.parent()

    @customOptions =
      draw:
        position: 'topleft'
        circle:     false
        marker:     false
        polygon:    false
        polyline:   false
        rectangle:  false
    inputDataOptions = @input.data 'draw'
    throw "Map Selector: please fill data: {draw: [marker/polygon/polyline... ]} in input attributes" if !inputDataOptions || inputDataOptions.length == 0
    for opt in inputDataOptions
      @customOptions.draw[opt] = true if @customOptions.draw[opt]?

    @drawItems = @createLayer()
    @input.attr 'type', 'hidden'
    @mapContainer = $("<div class='map-container'>").appendTo @el
    @initializeMap()
    @map.addLayer @drawItems
    @initializeDrawControl()
    # $(window).bind 'map:changeCenter', @paneUpdatedCenter

  mapOptions: ->
    zoom: 13
    center: [54.832479, 37.620977]
    imagePath: '/assets'
    attributionControl: false
    layers: [
      url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      options:
        tms: false
    ]

  drawControlOptions: =>
    edit:
      edit:
        title: 'Редактировать'
      featureGroup: @drawItems
      remove:
        title: 'Очистить'

  initializeMap: =>
    opt = @mapOptions()
    L.Icon.Default.imagePath = opt.imagePath

    layers = _(opt.layers).map (layer) ->
      new L.tileLayer layer.url, layer.options
    @map = L.map @mapContainer.get(0),
      zoom: opt.zoom
      layers: layers
      center: opt.center
      zoomControl: true
      scrollWheelZoom: false
      attributionControl: opt.attributionControl

    cadastre = new L.TileLayer.WMS 'http://maps.rosreestr.ru/arcgis/services/Cadastre/CadastreWMS/MapServer/WMSServer',
      format: 'image/png'
      tileSize: 512
      transparent: 'true'
      layers: '16,15,14,13,11,10,9,22,21,20,19,18,7,6'

    layersControl = new L.control.layers
    layersControl.addOverlay cadastre, 'Кадастр'
    layersControl.addTo @map

  initializeDrawControl: =>
    options = _.extend @drawControlOptions(), @customOptions
    drawControl = new L.Control.Draw options
    @map.addControl drawControl
    @map.on 'draw:created',   @onCreated
    @map.on 'draw:edited',    @onEdited
    @map.on 'draw:deleted',   @onDeleted
    drawControl

  # Layer handlers

  createLayer: =>
    coordinates = @input.val()
    res = new L.geoJson()
    if coordinates
      try
        obj = JSON.parse coordinates
        obj['type'] = 'FeatureCollection'
        geometries = obj['geometries']
        obj['features'] = for geometry in geometries
          type: 'Feature'
          properties: {}
          geometry: geometry
        delete obj['geometries']
        res = new L.geoJson obj
    res

  saveLayer: =>
    obj = @drawItems.toGeoJSON()
    obj['type'] = 'GeometryCollection'
    featuresArr = obj['features']
    geometries = for feature in featuresArr
      feature['geometry']
    obj['geometries'] = geometries
    delete obj['features']
    @input.val JSON.stringify(obj)

  onCreated: (event) =>
    layer = event.layer
    @drawItems.addLayer layer
    @saveLayer()

  onEdited: (event) => @saveLayer()

  onDeleted: (event) => @saveLayer()

  # # Этот мерзкий кусок кода нужно написать снова и по другому
  # # Возможно, стоит вообще всю механику пересмотреть [О.С.]
  # cityCoords: =>
  #   cityField = $(".coords-field", document)
  #   if cityField.length != 0
  #     if $("option:selected", cityField).data("coords")?
  #       cityCoords = $("option:selected", cityField).data("coords")["geometries"][0]["coordinates"]
  #     else
  #       cityCoords = [37.620393, 55.75396]
  #   else
  #     if $(@input).data("coords")?
  #       cityCoords = $(@input).data("coords")["geometries"][0]["coordinates"]
  #     else
  #       cityCoords = [37.620393, 55.75396]
  #   [cityCoords[1], cityCoords[0]]

  # paneUpdatedCenter: =>
  #   @map.panTo @cityCoords()

window.addComponent MapSelector, className: "map-selector"