class ResidentsSlider
  constructor: (el)->
    @el = $ el
    @nav = $ '.slider-nav', @el

    @el.slick
      lazyLoad: 'progressive'
      slidesToShow: 4
      slidesToScroll: 1
      arrows: true
      dots: false
      adaptiveHeight: false
      prevArrow: '<button type="button" class="slick-prev"></button>'
      nextArrow: '<button type="button" class="slick-next"></button>'
      responsive: [
          breakpoint: 1050
          settings:
            slidesToShow: 2
            centerMode: true
            centerPadding: '0px'
        ,
          breakpoint: 760
          settings:
            slidesToShow: 1
            centerMode: true
            centerPadding: '0px'
        ]


window.addComponent ResidentsSlider, className: 'residents-slider'