class Fancybox
  constructor: (el)->
    @$el = $ el

    @$el.find('.fancybox-slide').fancybox
      type: 'image'
      openEffect: 'none'
      closeEffect: 'none'

window.addComponent Fancybox, className: 'fancybox'