class MainSlider
  constructor: (el)->
    @el = $ el
    @nav = $ '.slider-nav', @el

    @el.slick
      lazyLoad: 'ondemand'
      slidesToShow: 1
      slidesToScroll: 1
      arrows: true
      dots: true
      adaptiveHeight: false
      prevArrow: '<button type="button" class="slick-prev"></button>'
      nextArrow: '<button type="button" class="slick-next"></button>'


window.addComponent MainSlider, className: 'main-slider'