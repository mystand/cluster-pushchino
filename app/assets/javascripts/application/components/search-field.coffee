class SearchField
  constructor: (el)->
    @$el = $ el

    searchRequest = () ->
      $.ajax({
        url : "/organizations",
        method: "GET",
        data: {'search': $(this).val()}
        success: (data)->
          console.log(data)
          $('.search-results').html(data);
      });

    throttled = _.throttle(searchRequest, 100);

    @$el.on 'keyup', throttled

window.addComponent SearchField, className: 'search-field'