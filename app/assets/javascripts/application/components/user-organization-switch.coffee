class UserOrganizationSwitch
  constructor: (el)->
    @el = $ el
    @chbx = $('input:checkbox', @el)
    @organizationSelect = $('.user_organization', @el)
    @organizationString = $('.user_outer_organization', @el)
    $(@chbx).bind 'change', @toggle
    @initialize()

  initialize: =>
    if ($(@chbx).prop('checked'))
      $(@organizationSelect).hide()
    else
      $(@organizationString).hide()

  toggle: (ev) =>
    $(@organizationString).toggle()
    $(@organizationSelect).toggle()

window.addComponent UserOrganizationSwitch, className: 'user-organization-switch'