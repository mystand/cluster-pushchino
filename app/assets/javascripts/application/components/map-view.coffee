leafletOptions =
  zoom: 8
  center: [47.233333, 47.233333]
  imagePath: '/assets'
  attributionControl: false
  scrollWheelZoom: false
  layers: [
    new L.tileLayer('https://b.tile.openstreetmap.org/{z}/{x}/{y}.png', {tms: false})
  ]

class MiniMap
  constructor: (el) ->
    @el = $ el
    L.Icon.Default.imagePath = '/assets'
    options =
      name: @el.data 'name'
      objId: @el.data 'obj-id'
      mapUrl: @el.data 'map-url'
      coords: @el.data 'coords'
      className: @el.data 'class-name'
      popupText: @el.data 'popup-text'
    @map = L.map el, leafletOptions
    bounds = L.geoJson(options.coords).getBounds()
    @map.fitBounds bounds
    @map.setZoom 12

    layer = new L.geoJson options.coords,
      pointToLayer: (featureData, latlng) ->
        icon = new L.Icon.Default()
        marker = L.marker latlng, {
          icon : icon
        }
        marker

    @map.addLayer layer
    for id, marker of layer._layers
      marker.bindPopup("#{options.popupText}").openPopup() if options.popupText

window.addComponent MiniMap, className: 'mini-map'
