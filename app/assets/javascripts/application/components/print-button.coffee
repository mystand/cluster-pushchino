class PrintButton
  constructor: (el)->
    @$el = $ el
    
    @$el.on 'click', (e) ->
      e.preventDefault()
      window.print()

window.addComponent PrintButton, className: 'print-button'