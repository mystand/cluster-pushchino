class GallerySlider
  constructor: (el)->
    @el = $ el

    @el.slick
      lazyLoad: 'progressive'
      slidesToShow: 3
      slidesToScroll: 1
      arrows: true
      dots: false
      adaptiveHeight: false
      centerMode: true
      variableWidth: true
      prevArrow: '<button type="button" class="slick-prev"></button>'
      nextArrow: '<button type="button" class="slick-next"></button>'
      responsive: [
        breakpoint: 1100
        settings:
          slidesToShow: 2
          centerMode: true
          centerPadding: '0px'
      ,
        breakpoint: 760
        settings:
          slidesToShow: 1
          centerMode: true
          centerPadding: '0px'
      ]

window.addComponent GallerySlider, className: 'gallery-slider'