$ ->
  # Это необходимо для того чтобы при удалении объектов с ошибкой валидации, они уже были удалены при отрисовки формы,
  # так мы исправляем некорректное поведение основной библиотеки
  nestedDestroyhiddenFields = $(".fields input[type=hidden][name*=_destroy]")
  nestedDestroyhiddenFields.each (i, field) ->
    if $(field).val() == 'true'
      $(field).parents(".nested-fields").first().find('a.remove_nested_fields').click()