window.initialize = (el = document) ->
  func? el for key, func of window.initializers

ready =  ->
  initialize()

$(document).ready(ready)

window.addComponent = (klass, options = {}) ->
  className = options.className
  id = options.id

  throw "options must have 'className' or 'id'" if !id? && !className?

  if className?
    initializedClassName = "#{className}-initialized"
    disabledInitializeClassName = "disable-initializer"
    disabledCurrentInitializeClassName = "disable-#{className}"
    window.initializers ||= {}
    window.initializers[className] = (el) ->
      targets = $ ".#{className}:not(.#{initializedClassName}, .#{disabledInitializeClassName}, .#{disabledCurrentInitializeClassName})", el
      targets.each (i, target) ->
        item = new klass target
        $(target).addClass initializedClassName
        options.handler? item

  if id?
    window.initializers ||= {}
    window.initializers["##{id}"] = (el) ->
      target = document.getElementById(id)
      new klass target if target?
