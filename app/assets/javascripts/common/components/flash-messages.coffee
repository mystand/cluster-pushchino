class FlashMessages
  constructor: (el)->
    @el = $ el
    @closeLink = $('.close-link', @el)
    @closeLink.bind 'click', @close
    setTimeout(@close, Math.max(@el.context.textContent.length*100, 3000))

  close: (ev) =>
    @el.addClass('hidden')
    no

window.addComponent FlashMessages, className: 'flash-messages'
