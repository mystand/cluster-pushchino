class ImperaviInputCustom
  constructor: (el)->
    @el = $ el
    csrf_token = $('meta[name=csrf-token]').attr('content')
    csrf_param = $('meta[name=csrf-param]').attr('content')
    if csrf_param && csrf_token then params = csrf_param + '=' + encodeURIComponent(csrf_token) else params = undefined

    @el.redactor
      focus: true
      formattingAdd:[ 
        {
          tag: 'p',
          title: 'Интервью: вопрос',
          class: 'interview-question',
          clear: true
        },
        {
          tag: 'p',
          title: 'Интервью: ответ',
          class: 'interview-answer',
          clear: true
        }
      ]
      lang: 'ru'
      imageUpload: '/admin/imperavi_pictures?' + params
      imageManagerJson: '/admin/imperavi_pictures.json'
      imageDestroy: '/admin/imperavi_pictures'
      fileUpload: '/admin/imperavi_attachments?' + params
      fileManagerJson: '/admin/imperavi_attachments.json'
      fileDestroy: '/admin/imperavi_attachments'
      buttons: ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule']
      plugins: ['table', 'imagemanager', 'filemanager']
      buttonSource: true
      changeCallback: =>
        @el.trigger 'change'

window.addComponent ImperaviInputCustom, className: 'imperavi-input-custom'