// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require select2
//= require select2_locale_ru
//= require jquery_nested_form

//= require underscore
//= require react
//= require react_ujs
//= require common/leaflet.js
//= require common/leaflet.draw.js
//= require_tree ../../../vendor/assets/javascripts/common
//= require_tree ../../../vendor/assets/javascripts/application

//= require ./common/initializer
//= require_tree ./common
//= require_tree ./application
//= require components
