window.VoteComponent = React.createClass({
    getInitialState: function () {
      var res = {};
      res.expand = this.props.expand;
      res.vote = JSON.parse(this.props.vote);
      res.messages = this.props.messages;

      res.votedOptions = this.props.voted_options;
      res.guest = !this.props.role || (["guest", "admin", "moderator"].indexOf(this.props.role) != -1) 
      return res;
    },

    expand: function () { 
      window.location.hash = "#" + this.state.vote.id;
      this.setState({ expand: true }) 
    },
    collapse: function () { this.setState({ expand: false }) },

    render: function () {
      var voteExpanded, overlay;
      var klass = "vote-container";
      if (this.state.expand) {
        klass += " expanded"
        voteExpanded = this.renderExpanded();
        overlay = <div className='vote-overlay' onClick={this.collapse}></div>
      }
      return <div className={klass} key={this.state.vote.id}>
        {overlay}
        {voteExpanded}
        {this.renderCollapsed()}
    </div>
    },

    renderCollapsed: function () {
      document.documentElement.style.overflow = "auto"
      document.body.style.overflow = "auto"
      var votedLabel;
      var klass = "vote-label";
      if (this.props.voted) {
        votedLabel = <div className="voted-label"></div>
        klass += " voted"
      }
      return <div className={klass} onClick={this.expand}>
        {votedLabel}
        <div className="date"></div>
        <div className="title">{this.state.vote.title}</div>
        <div className="already-voted-count">
          <div className="title">
            {this.state.messages.voted}:
          </div>
          <div className="value">{this.state.vote.organizations_count}</div>
        </div>
      </div>
    },

    renderExpanded: function () {
      document.documentElement.style.overflow = "hidden"
      document.body.style.overflow = "hidden"
      var form = <VoteForm vote={this.state.vote}
                           url={this.props.url}
                           collapse={this.collapse}
                           messages={this.props.messages}
                           organizations={this.props.organizations}
                           votedOptions={this.props.votedOptions}/>
      var results = <VoteResult vote={this.state.vote} votedOptions={this.state.votedOptions} collapse={this.collapse} />
      var alreadyVoted = <VoteInfo title={this.state.messages.already} collapse={this.collapse} />
      var notFinishedYet = <VoteInfo title={this.state.messages.not_finished} collapse={this.collapse} />
      var resultsClosed = <VoteInfo title={this.state.messages.view_closed} collapse={this.collapse} />

      var _A_ = form;
      var _B_ = results;
      var _C_ = alreadyVoted;
      var _D_ = notFinishedYet;
      var _E_ = resultsClosed;

      var _x_ = !this.state.vote.closed;
      var _y_ = this.props.voted;
      var _z_ = this.state.vote.show_results_immediately;
      var _a_ = !this.state.guest;

      if (_x_) {
        if (_a_) {
          if (_y_) {
            return _C_;
          } else {
            return _A_;
          }
        }
      } else {
        if (_z_) {
          return _B_;
        } else {
          return _E_;
        }
      }
      return _D_;
    }
});