window.VoteInfo = React.createClass({
  render: function () {
    return(
      <div className="vote-results-wrapper">
        <div className="inner-wrap">
          <div className="wrap">
            <div className="relative-wrap">
              <div className="vote vote-info">
                <div className="close" onClick={this.props.collapse}></div>
                <div className="title">{this.props.title}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
});