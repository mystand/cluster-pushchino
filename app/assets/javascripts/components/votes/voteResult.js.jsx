window.VoteResult = React.createClass({
  render: function () {
    var vote = this.props.vote;
    var votedOptionsMap = {}
    this.props.votedOptions.forEach(function (o) {
      votedOptionsMap[o.option_id] = o
    });
    var optionsMap = {};
    vote.options.forEach(function (opt) {
      optionsMap[opt.id] = opt.title
    });

    var maxValue = Math.max.apply(null, _(vote.results).values());
    var lines = {};
    _(vote.results).each(function (value, key) {
      lines[key] = 100 * value / maxValue;
    });
    var votedLabel;
    if (this.props.votedOptions.length > 0) {
      votedLabel = <div className="voted-label"></div>
    }

    return(
      <div className="vote-results-wrapper">
        <div className="inner-wrap">
          <div className="wrap">
            <div className="relative-wrap">
              <div className="vote vote-results">
                {votedLabel}
                <div className="close" onClick={this.props.collapse}></div>
                <div className="title">{vote.title}</div>
                <div className="description" dangerouslySetInnerHTML={ { __html: vote.description } }></div>
                <div className="results">
                  {
                    _(vote.results).map(function (val, key) {
                      var klass = "result"
                      if (votedOptionsMap[key]) klass += " voted-result"
                      return <div key={key} className={klass}>
                        <div className="title">{optionsMap[key]}</div>
                        <div className="line" style={{ width: lines[key] + "%" }}></div>
                        <div className="value">{val || 0}</div>
                      </div>
                    })
                  }
                </div>
                <div className="already-voted-count">
                  <div className="title">
                    Проголосовало:
                  </div>
                  <div className="value">{vote.organizations_count}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
});