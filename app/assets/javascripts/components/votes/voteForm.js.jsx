window.VoteForm = React.createClass({
  getInitialState: function () {
    var res = {};
    res.vote = this.props.vote;
    res.voteData = {};
    res.vote.options.forEach(function (option) {
      res.voteData[option.id] = false;
    });
    res.organizations = this.props.organizations;
    res.organizationData = {};
    res.organizations.forEach(function (organization) {
      res.organizationData[organization.id] = false;
    });
    return res;
  },

  toggleOption: function (id) {
    var voteData = this.state.voteData;
    voteData[id] = !voteData[id];
    this.setState({ voteData: voteData });
  },

  toggleRadioOption: function (id) {
    var voteData = this.state.voteData;
    _(voteData).each(function (value, key) {
      voteData[key] = (key == id) ? !voteData[id] : false;
    });
    this.setState({ voteData: voteData });
  },

  toggleOrganization: function (id) {
    var organizationData = this.state.organizationData;
    organizationData[id] = !organizationData[id];
    this.setState({ organizationData: organizationData });
  },

  voteSubmit: function (e) {
    e.preventDefault();
    var data = {
      vote_id: this.state.vote.id
    };

    data.options = _(this.state.voteData).chain().map(function (value, key) {
      if (value) return key;
    }).compact().value();
    if (this.state.organizations.length>1) {
      data.organizations = _(this.state.organizationData).chain().map(function (value, key) {
          if (value) return key;
      }).compact().value();
    }
    else data.organizations = [this.state.organizations[0].id];
    $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      },
      type: "POST",
      url: this.props.url,
      data: data
    }).then(function () {
      console.log("tHEN");
      window.location.reload()
    })
  },

  render: function () {
    var vote = this.state.vote;
    var voteOptionsBlock;

    if (vote.allow_multiple_answer) {
      voteOptionsBlock = vote.options.map(function (option) {
        var id = "vote_option_" + option.id;
        var className = 'checkbox';
        if (this.state.voteData[option.id]) className += ' checked';
        return <div key={id} className='vote-option' onClick={_.partial(this.toggleOption, option.id)}>
          <div className={className}></div>
          <label >{option.title}</label>
        </div>
      }.bind(this))
    } else {
      voteOptionsBlock = vote.options.map(function (option) {
        var id = "vote_option_" + option.id;
        var className = 'radio';
        if (this.state.voteData[option.id]) className += ' active';
        return <div key={id} className='vote-option' onClick={_.partial(this.toggleRadioOption, option.id)}>
          <div className={className}></div>
          <label>{option.title}</label>
        </div>
      }.bind(this));
    }

    var votedOrganizations = this.props.votedOptions.map(function (option) {
        return option.organization_id;
    });
    var organizationList = this.state.organizations.map(function (organization) {
        var id = "organization_" + organization.id;
        var className = 'checkbox';
        if (this.state.organizationData[organization.id]) className += ' checked';
        if (votedOrganizations.indexOf(organization.id) != -1) {
            return <div key={id} className='vote-option disabled' onClick={null}>
              <div className={className}></div>
              <label>{organization.name}</label>
            </div>
        }
        return <div key={id} className='vote-option' onClick={_.partial(this.toggleOrganization, organization.id)}>
          <div className={className}></div>
          <label>{organization.name}</label>
        </div>
    }.bind(this));

    var selectOrganizationBlock = <div className="organizations">
      <div>{this.props.messages.select_organizations}:</div>
      <div className="organization-list">{organizationList}</div>
    </div>;

    return(
      <div className="vote-results-wrapper">
        <div className="inner-wrap">
          <div className="wrap">
            <div className="relative-wrap">
              <div className="vote">
                <div className="close" onClick={this.props.collapse}></div>
                <div className="title">{vote.title}</div>
                <div className="description" dangerouslySetInnerHTML={ { __html: vote.description } }></div>
                {
                  (vote.allow_multiple_answer) &&
                    <div className="hint">{this.props.messages.hint}</div>
                }
                <div className="options">
                  <form onSubmit={this.voteSubmit} className="simple_form /ru/user_options" action="/ru" method="post">
                    {voteOptionsBlock}
                    <input type="hidden" name="/ru/user_options[option_ids][]" value="" />
                    {
                      this.state.organizations.length>1 &&
                        selectOrganizationBlock
                    }
                    <div className="submit">
                      <input type="submit" name="commit" value={this.props.messages.submit} />
                    </div>
                  </form>
                </div>
                <div className="already-voted-count">
                  <div className="title">{this.props.messages.voted}:</div>
                  <div className="value">{vote.organizations_count}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
});