//= require jquery
//= require jquery_ujs
//= require jquery_nested_form
//= require select2
//= require select2_locale_ru

//= require common/leaflet.js
//= require common/leaflet.draw.js
//= require common/underscore.js
//= require admin/underscore.string.js
//= require_tree ../../../vendor/assets/javascripts/common
//= require_tree ../../../vendor/assets/javascripts/admin

//= require ./common/initializer
//= require_tree ./common
//= require_tree ./admin