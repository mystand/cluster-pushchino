class LocalizedImperaviInput < LocalizedTextInput
  def input(wrapper_options)
    input_html_options[:class].push 'imperavi-input'
    super
  end
end
class ImageInput < SimpleForm::Inputs::FileInput
  def input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    out = ''
   
    if object.send(attribute_name)
      out << template.link_to(image_url, class: 'img-thumbnail') do
        template.image_tag(image_url)
      end
    end

    out << template.content_tag(:div)
    out << @builder.file_field(attribute_name, merged_input_options)

    cache_method = "#{attribute_name}_cache"

    if @builder.object.respond_to? cache_method
      out << @builder.hidden_field(cache_method, merged_input_options) #carrierwave
    else
      out << @builder.hidden_field("retained_#{attribute_name}", merged_input_options) #dragonfly
    end
    if object.send(attribute_name)
      out << @builder.check_box("remove_#{attribute_name}", class: 'delete-checkbox')
      out << template.content_tag(:span, t("helpers.delete"), class: 'delete-checkbox-label')
    end
    out
  end

  private

  def image_url
    if object.send(attribute_name).respond_to? :thumb
      object.send(attribute_name).thumb('110x90').url
    else
      object.send(attribute_name).url
    end
  end
end