class ImperaviMailerInput < SimpleForm::Inputs::TextInput
  def input(wrapper_options)
    input_html_options[:class].push 'imperavi-mailer-input'
    super
  end
end
