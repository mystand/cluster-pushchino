class LocalizedImperaviInput < LocalizedTextInput
  def input(wrapper_options)
    input_html_options[:class].push 'imperavi-input'
    super
  end
end
