class LocalizedImperaviCustomInput < LocalizedTextInput
  def input(wrapper_options)
    input_html_options[:class].push 'imperavi-input-custom'
    super
  end
end
