class ContactsController < ApplicationController
  def index
    @pf_contacts = PageFragment.key("contacts").first
    @resource = Contact.new
  end

  def create
    @resource = Contact.new(resource_params)
    if @resource.save
      flash[:notice] = t('application.contacts.success')
      redirect_to action: :index
    else
      @pf_contacts = PageFragment.key("contacts").first
      render :index
    end
  end

  private

  def resource_params
    params.require(:contact).permit(:email, :name, :subject, :body)
  end
end