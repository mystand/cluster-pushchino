class OrganizationsController < ApplicationController

  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    @collection = Organization.search(params[:search]).order("name->'"+I18n.locale.to_s+"' ASC").page(params[:page]).per(12)
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.organizations.index.title') }]
    if request.xhr?
      render partial: 'search_results'
    end
  end

  def show
    build_breadcrumbs
  end

  def edit
    build_breadcrumbs
  end

  def update
    if @resource.update(organization_params)
      redirect_to @resource, notice:I18n.t('views.organizations.flash.successfully_updated')
    else
      render :edit
    end
  end

  private

  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += [{ name: I18n.t('views.main.residents.title'), url: organizations_path }]

    @breadcrumbs.push name: @resource.name

    @breadcrumbs
  end

  def attachments_permit
    {organization_attachments_attributes: [:id, :_destroy, :remove_organization_attachment, 
    :organization_attachment_cache, :organization_id, :description] + fp(:file)}
  end

  def organization_params
    params.require(:organization).permit(attachments_permit)
  end
end