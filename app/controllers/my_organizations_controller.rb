class MyOrganizationsController < ApplicationController
  def index
    @collection = current_user.organizations.page(params[:page]).per(12)
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.my_organizations.index.title') }]
  end
end
