class PostsController < ApplicationController

  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    filtered_collection = Post.published.desc
    used_years = filtered_collection.group_by { |m| m.publish_date.beginning_of_year.year }.keys
    @year = (params['by_year'] || used_years[0] || Time.zone.now.year).to_i
    @showed_months = filtered_collection.by_year(@year).group_by { |m| m.publish_date.beginning_of_month.month }.keys
    @month = (params['by_month'] || @showed_months[0]).to_i
    @collection = filtered_collection.by_year(@year).by_month(@month).page(params[:page]).per(2)
    @prev_years = used_years.select {|y| y < @year}
    @next_years = used_years.select {|y| y > @year}
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.main.posts.title') }]
  end

  def show
    @other_posts = Post.published.desc.except_id(params[:id]).limit(10)
    @interviews = Interview.published.desc
    build_breadcrumbs
  end

  private


  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += [{ name: I18n.t('views.main.posts.title'), url: posts_path }]

    @breadcrumbs.push name: @resource.title

    @breadcrumbs
  end
end