class InterviewsController < ApplicationController

  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    @collection = Interview.published.desc.page(params[:page]).per(5)
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.main.interviews.title') }]
  end

  def show
    # @resource = Interview.find params[:id]
    build_breadcrumbs
  end

  private

  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += [{ name: I18n.t('views.main.interviews.title'), url: interviews_path }]

    @breadcrumbs.push name: @resource.title

    @breadcrumbs
  end
end