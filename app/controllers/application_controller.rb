class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale
  before_action :set_font_size
  before_action :load_settings
  before_action :load_footer_contacts
  
  rescue_from CanCan::AccessDenied, with: :render_access_denied

  def render_access_denied
    flash[:error] = t('views.admin.access_denied_service')
    redirect_to root_path
  end


  def after_sign_in_path_for(resource)
    stored_location_for(resource) || root_path
    # if resource.is_a?(User)
    #   if (resource.role?(:admin) || resource.role?(:moderator))
    #     admin_root_path
    #   else
    #     root_path
    #   end  
    # else
    #   root_path
    # end
  end

  def default_url_options(*)
    { locale: I18n.locale }
  end

  def set_locale
    locale = params[:locale] || session[:locale] || I18n.default_locale
    session[:locale] = locale
    I18n.locale = locale
  end

  def set_font_size
    @font_size = params[:font_size] || session[:font_size]
    @font_size = nil unless ['small', 'medium', 'big'].include?(@font_size)
    session[:font_size] = @font_size
  end

  def load_settings
    @settings = Settings.first
  end

  def load_footer_contacts
    @pf_footer_contacts = PageFragment.key("footer_contacts").first
  end

  def root_breadcrumb
    { name: I18n.t('application.navigation.root'), url: root_path }
  end  

  private

  def file_permit(param)
    [param.to_sym, "#{param}_uid".to_sym, "#{param}_name".to_sym, "remove_#{param}".to_sym, "retained_#{param}".to_sym]
  end
  alias_method :fp, :file_permit

  def gallery_permit
    {pictures_attributes: [:id, :_destroy, :remove_picture, :picture_cache, :target_id, :target_type, :title, :description] + fp(:file)}
  end

  def image_permit(param)
    [param.to_sym, "#{param}_uid".to_sym, "#{param}_name".to_sym, "remove_#{param}".to_sym, "retained_#{param}".to_sym]
  end
end