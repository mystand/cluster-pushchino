class MessagesController < ApplicationController

  layout 'application'

  before_action :authenticate_user!
  before_action :authorize_user!

  before_action :set_message, only: [:edit, :update, :destroy]

  load_and_authorize_resource

  def edit
    build_breadcrumbs
  end

  def create
    @message = Message.new(message_params.merge(user: current_user))
    authorize! :create, @message
    @topic = @message.topic
    
    if @message.save
      redirect_to @topic, notice: t('views.messages.flash.successfully_created')
    else
      render 'topics/show'
    end
  end

  def update
    @topic = Topic.find(@message.topic_id)
    @message.assign_attributes(message_params.merge(editor: current_user))

    if @message.save
      redirect_to @topic, notice: t('views.messages.flash.successfully_updated')
    else
      render :edit
    end
  end

  def destroy
    @topic = Topic.find(@message.topic_id)
    @message.destroy
    redirect_to @topic
  end

  protected

  def authorize_user!
    # raise CanCan::AccessDenied if current_user.nil? || current_user.role?(:client) || current_user.role?(:normal)
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:user_id, :topic_id, :text)
  end

  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += [{ name: I18n.t('views.topics.index.title'), url: topics_path }]
    @breadcrumbs += [{ name: @message.topic.title, url: topic_path(@message.topic) }]

    @breadcrumbs.push name: t("helpers.edit_message")
    @breadcrumbs
  end
end
