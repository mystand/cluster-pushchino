class HomeController < ApplicationController
  layout 'application'

  def index
    @vote = Vote.accessible_by(current_ability).published.where(closed: false).order(:date_start).try(:first) if current_user.present?
    if @vote.blank?
      @posts = Post.published.desc.limit(4)
    else
      @posts = Post.published.desc.limit(2)
    end
    @interview = Interview.published.desc.limit(1).first
    @pf_main_about = PageFragment.key("main_page_about").first
    @slides = Slide.published.desc
    @residents = Organization.in_slider.limit(50)
  end

  def greeting
    @breadcrumbs = [root_breadcrumb] + [{ name: @settings.greeting_title}]
  end
end
