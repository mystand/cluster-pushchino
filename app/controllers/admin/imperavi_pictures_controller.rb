class Admin::ImperaviPicturesController < Admin::AdminController
  def destroy
    ImperaviPicture.find(params[:id]).destroy
    request.xhr? ? render(text: true) : redirect_to(admin_imperavi_pictures_path)
  end

  def create
    @picture = ImperaviPicture.create file: params[:file]
    render json: { filelink: @picture.file.try(:url), filename: '', id: @picture.id.to_s }
  end

  def index
    res = ImperaviPicture.where(target: nil).map do |p|
      { thumb: p.file.thumb('100x75#').url, image: p.file.try(:url), title: '', id: p.id.to_s }
    end
    render json: res
  end
end