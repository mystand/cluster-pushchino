class Admin::AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user!
  layout 'admin'
  respond_to :html
  responders :flash, :http_cache, :admin_collection
  protect_from_forgery with: :exception

  protected

  def authorize_user!
    raise CanCan::AccessDenied if current_user.nil? || current_user.role?(:guest) || current_user.role?(:normal)
  end

  def localized_permit name
    obj = {}
    obj[name] = I18n.available_locales
    obj
  end
  alias_method :lp, :localized_permit

  def path_with_continue(options = {})
    location = options[:location]
    scopes = options[:scopes] || []
    resource = options[:resource]
    return [:edit, :admin] + scopes + [resource] if resource.errors.any?
    return [:edit, :admin] + scopes + [resource] if params[:continue]
    res = [:admin]
    res += scopes
    res.push resource
    res.push(location: location) if location
    res
  end

end
