class Admin::ImperaviAttachmentsController < Admin::AdminController
  include ActionView::Helpers::NumberHelper

  def destroy
    ImperaviAttachment.find(params[:id]).destroy
    request.xhr? ? render(text: true) : redirect_to(admin_imperavi_attachments_path)
  end

  def create
    filename = params[:file].try(:original_filename) || 'file'
    @attachment = ImperaviAttachment.create file: params[:file]
    render json: { filelink: @attachment.file.try(:url), filename: filename, id: @attachment.id.to_s }
  end

  def index
    res = ImperaviAttachment.where(target: nil).map do |p|
      { size: number_to_human_size(p.file.size), link: p.file.try(:url), id: p.id.to_s, name: File.extname(p.file_name), title: File.basename(p.file_name, '.*' ) }
    end
    render json: res
  end
end