class Admin::PresentationsController < Admin::AdminController
  before_action :set_breadcrumbs, except: [:index, :destroy]
  load_and_authorize_resource instance_name: :resource

  def index
    @breadcrumbs = [{ name: I18n.t('admin.presentations.index.title') }]
    @collection = Presentation.page(params[:page]).per(10)
    respond_with(:admin, @collection)
  end

  def create
    @resource = Presentation.new(resource_params)
    @resource.save
    @resource.menu_item = MenuItem.find params[:menu_item_id] if params[:menu_item_id].present?
    respond_with(*path_with_continue(resource: @resource, location: admin_menu_items_path))
  end

  def update
    @resource.update(resource_params)
    respond_with(*path_with_continue(resource: @resource, location: admin_menu_items_path))
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  private

  def set_breadcrumbs
    @breadcrumbs = [{ name: I18n.t('admin.presentations.index.title'), url: admin_presentations_path },
                    { name: t('.title') }]
  end

  def resource_params
    # allowed = [
    #     lp(:title),
    #     lp(:description),
    #     lp(:text),
    #     image_permit(:image),
    #     gallery_permit,
    #     :published_at,
    #     :published,
    #     :international_event
    # ]

    # if can? :moderate, @resource
    #   allowed << :published
    # end

    params.require(:presentation).permit! #(*allowed)
  end
end