class Admin::SettingsController < Admin::AdminController
  before_action :set_settings, only: [:show, :edit, :update]

  def show
  end

  def create
    Settings.new(settings_params)
    @settings.save!
    respond_with(:admin, @settings, location: admin_settings_path)
  end

  def update
    @settings.update(settings_params)
    @settings.save!
    respond_with(:admin, @settings, location: admin_settings_path)
  end

  private

  def settings_params
    params.require(:settings).permit  lp(:site_name),
                                      :contact_phone_number,
                                      :contact_email,
                                      :ig_url,
                                      :vk_url,
                                      :fb_url,
                                      :ln_url,
                                      :default_signature,
                                      image_permit(:greeting_image),
                                      lp(:greeting_title),
                                      lp(:greeting_short_description),
                                      lp(:greeting_paragraph_1),
                                      lp(:greeting_paragraph_2),
                                      user_notification_user_ids: [],
                                      vote_notification_user_ids: [],
                                      feedback_notification_user_ids: []
  end

  def set_settings
    @settings = Settings.first
  end
end