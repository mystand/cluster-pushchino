class Admin::PostsController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    @collection = apply_scopes(Post).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Post.new
  end

  def create
    @resource = Post.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    params.require(:post).permit(lp(:title), lp(:intro), image_permit(:image), lp(:content), gallery_permit, :publish_date, :published, :approved)
  end
end
