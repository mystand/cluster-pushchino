class Admin::SlidesController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(Slide).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Slide.new
  end

  def create
    @resource = Slide.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    params.require(:slide).permit(lp(:title), lp(:subtitle), image_permit(:image), :published)
  end
end
