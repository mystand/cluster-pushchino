class Admin::OrganizationsController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource, find_by: :url

  has_scope :by_query, as: :query

  def index
    @collection = apply_scopes(Organization).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Organization.new
  end

  def create
    @resource = Organization.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def attachments_permit
    {organization_attachments_attributes: [:id, :_destroy, :remove_organization_attachment, 
    :organization_attachment_cache, :organization_id, :description] + fp(:file)}
  end

  def resource_params
    allowed = [
      lp(:name), lp(:chief_name), lp(:competence),
      lp(:capabilities), lp(:offers), lp(:interests),
      lp(:contacts), lp(:address), :website,
      :coordinates_geojson, :in_slider, attachments_permit,
      image_permit(:logo), gallery_permit,
      activity_type_ids: []
    ]

    params.require(:organization).permit(*allowed)
  end
end
