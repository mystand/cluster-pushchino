class Admin::InterviewsController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    @collection = apply_scopes(Interview).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Interview.new
  end

  def create
    @resource = Interview.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    params.require(:interview).permit(lp(:title), lp(:intro), image_permit(:image), gallery_permit, lp(:content), :publish_date, :published)
  end
end