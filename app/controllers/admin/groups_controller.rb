class Admin::GroupsController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(Group).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Group.new
  end

  def create
    @resource = Group.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    params.require(:group).permit(:title, user_ids: [])
  end
end
