class Admin::ActivityTypesController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(ActivityType).page(params[:page])
    respond_with @collection
  end

  def new
    @resource = ActivityType.new
  end

  def create
    @resource = ActivityType.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    params.require(:activity_type).permit(lp(:name))
  end
end