class Admin::ContactsController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(Contact).page(params[:page])
    respond_with @collection
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end
end