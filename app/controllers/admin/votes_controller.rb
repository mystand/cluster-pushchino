class Admin::VotesController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(Vote).order("closed, date_end desc").page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Vote.new
  end

  def create
    @resource = Vote.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    option_ids = (@resource.try(:option_ids) || []).map(&:to_s).map(&:to_sym)
    params.require(:vote).permit(lp(:title), {:results => option_ids},
       lp(:description), :date_start, :date_end, :closed,
      :published, :show_results_immediately, :allow_multiple_answer, 
      options_attributes: [:id, lp(:title), lp(:description), :_destroy],
      group_ids: [], user_ids: []
    )
  end
end
