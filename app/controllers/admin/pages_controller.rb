class Admin::PagesController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource, find_by: :url_or_id
  before_action :set_resource_breadcrumbs, except: [:index, :destroy]

  def new
    @resource = Page.new
  end

  def edit
  end

  def index
    @breadcrumbs = [{name: I18n.t('admin.pages.index.title')}]
    @collection = apply_scopes(Page.accessible_by(current_ability)).page(params[:page]).per(10)
    respond_with(@collection)
  end

  def create
    @resource = Page.new resource_params
    @resource.save
    @resource.menu_item = MenuItem.find params[:menu_item_id] unless params[:menu_item_id].blank?
    respond_with *path_with_continue(resource: @resource)
  end

  def update
    @resource.update resource_params
    respond_with *path_with_continue(resource: @resource)
  end

  def destroy
    @resource.destroy
    respond_with :admin, @resource
  end

  private

  def set_resource_breadcrumbs
    @breadcrumbs = [
        {name: I18n.t('admin.pages.index.title'), url: admin_pages_path},
        {name: t('.title')}
    ]
  end

  def resource_params
    a = [lp(:intro), lp(:content), lp(:title)]
    params.require(:page).permit(*a)
  end
end