class Admin::MailingsController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource
  before_filter :fill_variables
  
  def index
    @collection = apply_scopes(Mailing).order("updated_at desc").page(params[:page])
    respond_with @collection
  end

  def new
    @resource = Mailing.new
  end

  def create
    @resource = Mailing.new(resource_params)
    @resource.save
    if params[:mailing][:status]
      params[:continue] = true
    end 
    respond_with(:admin, @resource)
  end

  def update
    if params[:repeat]
      new_resource = @resource.repeat!
      redirect_to edit_admin_mailing_path(new_resource)
    else 
      @resource.update(resource_params)
      if params[:mailing][:status]
        params[:continue] = true
      end    
      respond_with(:admin, @resource)
    end
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def attachments_permit
    {mailing_attachments_attributes: [:id, :_destroy, :remove_mailing_attachment, 
    :mailing_attachment_cache, :mailing_id] + fp(:file)}
  end

  def resource_params
    params.require(:mailing).permit(:title, :body, :signature, :status, attachments_permit, group_ids: [], user_ids: [])
  end

  private

  def fill_variables
    @groups = Group.accessible_by(current_ability)
  end
end
