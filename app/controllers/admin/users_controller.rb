class Admin::UsersController < Admin::AdminController
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = User.all.order('last_name, first_name, middle_name asc').page(params[:page])
    respond_with @collection
  end

  def new
    @resource = User.new
  end

  def create
    @resource = User.new(resource_params)
    @resource.save
    respond_with(:admin, @resource)
  end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  def destroy
    @resource.destroy
    respond_with(:admin, @resource)
  end

  def resource_params
    pp = [
      :email, 
      :first_name, :middle_name, :last_name, 
      :post, :approved,
      :outer_organization,
      image_permit(:image),
      organization_ids: []
    ]
    pp.push :role if current_user.role? :admin
    pp += [:password, :password_confirmation] unless params[:user][:password].blank?
    params.require(:user).permit(*pp)
  end
end
