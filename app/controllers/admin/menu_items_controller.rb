class Admin::MenuItemsController < Admin::AdminController
  load_and_authorize_resource instance_name: :menu_item, find_by: :semantic_url
  skip_load_resource only: :move
  before_action :set_breadcrumbs, except: [:index, :destroy]
  # include Scopable

  def index
    @breadcrumbs = [{name: I18n.t('views.admin.menu_items.index.title')}]
    respond_to do |format|
      format.json do
        render json: MenuItem.order(:created_at).all
      end
      format.html
    end
  end

  #PUT
  # params {id, target_id, position}
  def move
    leaf       = MenuItem.find params[:id]
    target     = MenuItem.find params[:target_id]
    old_parent = leaf.parent

    if params[:position] == 'inside'
      new_parent = target
      new_index  = new_parent.children_ids.count
    else
      if params[:position] != 'after' && params[:position] != 'before'
        raise "Unknown position #{params[:position]}"
      end
      new_parent = target.parent
      new_index = new_parent.child_id_index target.id
      new_index += 1 if params[:position] == 'after'
    end

    MenuItem.transaction do
      leaf.update_attributes! parent: new_parent
      old_parent.remove_child_id! leaf.id
      new_parent.reload.add_child_id! leaf.id, new_index
    end

    render json: {id: new_parent.id, children_ids: new_parent.reload.children_ids}
  end

  def edit
  end

  def update
    @menu_item.update(resource_params)
    if request.xhr?
      render json: {ok: true}
    else
      respond_with(*path_with_continue(resource: @menu_item))
    end
  end


  def create
    @menu_item = MenuItem.new resource_params
    MenuItem.transaction do
      @menu_item.save!
      @menu_item.parent.try :add_child_id!, @menu_item.id
    end
    res = @menu_item.attributes
    res[:parent_children_ids] = @menu_item.parent.reload.children_ids
    res[:children_ids] = @menu_item.children_ids
    res[:parent_id] = @menu_item.parent_id
    res[:target_name] = @menu_item.target.title if @menu_item.target
    render json: res
  end

  def destroy
    target = @menu_item
    MenuItem.transaction do
      target.parent.try :remove_child_id!, target.id
      target.destroy!
    end

    parent = target.parent.reload
    render json: {children_ids: parent.children_ids, id: parent.id}
  end

  def set_breadcrumbs
    @breadcrumbs = [{name: I18n.t('views.admin.menu_items.index.title'), url: admin_menu_items_path}, {name: t('.title')}]
  end

  def resource_params
    a = [ :parent_id, :url, :target_id, :key, lp(:annotation), lp(:title), :published,
          :breadcrumbs, :show_children, :target_type, {children_ids: []}, image_permit(:icon), gallery_permit]

    if can? :moderate, @menu_item
      a << :published
    end

    params.require(:menu_item).permit(*a)
  end

end
