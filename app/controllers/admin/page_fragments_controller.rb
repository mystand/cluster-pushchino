class Admin::PageFragmentsController < Admin::AdminController  
  load_and_authorize_resource instance_name: :resource

  def index
    @collection = apply_scopes(PageFragment).page(params[:page])
    respond_with @collection
  end

  # def new
  #   @resource = PageFragment.new
  # end

  # def create
  #   @resource = PageFragment.new(resource_params)
  #   @resource.save
  #   respond_with(:admin, @resource)
  # end

  def update
    @resource.update(resource_params)
    respond_with(:admin, @resource)
  end

  # def destroy
  #   @resource.destroy
  #   respond_with(:admin, @resource)
  # end

  def resource_params
    params.require(:page_fragment).permit(:name, :key, lp(:content))
  end
end