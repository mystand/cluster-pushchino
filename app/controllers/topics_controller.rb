class TopicsController < ApplicationController
  layout 'application'

  before_action :authenticate_user!
  before_action :authorize_user!

  before_action :set_topic, only: [:show, :edit, :update, :destroy, :archive, :unarchive]
  before_action :set_messages, only: [:show, :edit]
  before_action :set_new_message, only: [:show, :edit]

  def index
    @topics = Topic.accessible_by(current_ability).not_archived.by_last_message_at.page(params[:page]).per(10)
  end

  def archived
    @topics = Topic.accessible_by(current_ability).archived.by_last_message_at.page(params[:page]).per(10)
    render template: "topics/index"
  end

  def show
    build_breadcrumbs
  end

  def new
    @topic = Topic.new
    @groups = Group.accessible_by(current_ability)
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.topics.index.title'), url: topics_path }, { name: t('views.topics.new.title') }]
  end

  def edit
    @groups = Group.accessible_by(current_ability)
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.topics.index.title'), url: topics_path }, { name: t('views.topics.edit.title') }]
  end

  def create
    @topic = Topic.new(topic_params.merge(user: current_user))
    authorize! :create, @topic
    @groups = Group.accessible_by(current_ability)

    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: t('views.topics.flash.successfully_created') }
        format.json { render :show, status: :created, location: @topic }
      else
        format.html { render :new }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize! :update, @topic
    @groups = Group.accessible_by(current_ability)
    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to @topic, notice: t('views.topics.flash.successfully_updated') }
        format.json { render :show, status: :ok, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :destroy, @topic
    @topic.destroy
    
    respond_to do |format|
      format.html { redirect_to topics_url, notice: t('views.topics.flash.successfully_destroyed') }
      format.json { head :no_content }
    end
  end

  def archive
    authorize! :update, @topic
    respond_to do |format|
      if @topic.archive!
        format.html { redirect_to topics_url, notice: t('views.topics.flash.archived') }
        format.json { render :show, status: :ok, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def unarchive
    authorize! :update, @topic
    respond_to do |format|
      if @topic.unarchive!
        format.html { redirect_to topics_url, notice: t('views.topics.flash.unarchived') }
        format.json { render :show, status: :ok, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  protected

  def authorize_user!
    # raise CanCan::AccessDenied if current_user.nil? || current_user.role?(:client) || current_user.role?(:normal)
  end

  private

  def set_topic
    @topic = Topic.find(params[:id])
  end

  def set_messages
    @messages = Message.where(topic: @topic)
  end

  def set_new_message
    @message = Message.new(topic: @topic, user: current_user)
  end

  def topic_params
    params.require(:topic).permit(:title, :is_archieved, :topic_type, :user_id, :text, group_ids: [], user_ids: [])
  end

  def message_params
    params.require(:message).permit(:text)
  end

  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += [{ name: I18n.t('views.topics.index.title'), url: topics_path }]

    @breadcrumbs.push name: @topic.title

    @breadcrumbs
  end
end
