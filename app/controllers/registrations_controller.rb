class RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: :create
  before_action :configure_account_update_params, only: :update
  
  layout 'application'

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up,
      keys: [:first_name, :middle_name, :last_name, :post, 
             :organization_id, :organization_not_in_list, :outer_organization, image_permit(:image)]
    )
  end

  def configure_account_update_params
    keys = [:first_name, :middle_name, :last_name, :post, :outer_organization, image_permit(:image)]
    keys += [:organization_id] if current_user.role? :admin
    devise_parameter_sanitizer.permit(:account_update, keys: keys)
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
    current_ability = Ability.new(self.resource)
    raise CanCan::AccessDenied unless current_ability.can? :update, self.resource 
    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end
  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  def image_permit(param)
    [param.to_sym, "#{param}_uid".to_sym, "#{param}_name".to_sym, "remove_#{param}".to_sym, "retained_#{param}".to_sym]
  end
end
