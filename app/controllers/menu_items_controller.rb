class MenuItemsController < ApplicationController

  load_and_authorize_resource instance_name: :menu_item, find_by: :semantic_url

  def show
    # @menu_item = MenuItem.find params[:id]

    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += @menu_item.parent_list(no_root: true, breadcrumbs: true).map { |p| { name: p.title, url: view_context.menu_item_url(p) } }.reverse
    @breadcrumbs.push name: @menu_item.title
  end
end
