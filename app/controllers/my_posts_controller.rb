class MyPostsController < ApplicationController
  respond_to :html
  before_action :authenticate_user!
  load_and_authorize_resource :post

  def index
    @collection = Post.where.not(user_id: nil).accessible_by(current_ability).desc.page(params[:page]).per(2)
    @breadcrumbs = [root_breadcrumb, { name: I18n.t('views.main.my_posts.title'), url: my_posts_path }]
  end

  def new
    @resource = Post.new
    @breadcrumbs = [root_breadcrumb, { name: I18n.t('views.main.my_posts.title'), url: my_posts_path },
                                     { name: I18n.t('views.main.my_posts.new'), url: new_my_post_path }]
  end

  def create
    @resource = Post.new(resource_params)
    @resource.user = current_user
    @resource.save
    @breadcrumbs = [root_breadcrumb, { name: I18n.t('views.main.my_posts.title'), url: my_posts_path },
                                     { name: I18n.t('views.main.my_posts.new'), url: new_my_post_path }]
    respond_with(@resource, location: my_posts_path)
  end

  private

  def resource_params
    params.require(:post).permit(:title, :intro, image_permit(:image), :content, :publish_date)
  end

end