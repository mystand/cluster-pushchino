class VotesController < ApplicationController
  def index
    @opened = Vote.accessible_by(current_ability).published.where(closed: false).order(:date_start).preload(:options)
    @closed = Vote.accessible_by(current_ability).published.where(closed: true).order(:date_start).preload(:options)
    @expand = Vote.accessible_by(current_ability).published.find params[:expand] if params[:expand]
    @breadcrumbs = [root_breadcrumb] + [{ name: I18n.t('views.main.votes.title') }]
  end

  def vote
    vote = Vote.accessible_by(current_ability).published.where(closed: false).find params[:vote_id]
    authorize! :vote, vote
    options = params[:options]
    organization_ids = params[:organizations]
    res = {errors: []}
    status = 200
    organization_ids.each do |organization_id|
      options.each do |option_id|
        user_option = UserOption.new option_id: option_id,
                                     user_id: current_user.id,
                                     organization_id: organization_id,
                                     vote: vote
        unless user_option.save
          res[:errors].push(user_option.errors.messages)
          status = 406
        end
      end
    end
    render json: res, status: status
  end
end