class PagesController < ApplicationController

  load_and_authorize_resource instance_name: :resource, find_by: :url

  def index
    @collection = Page.all
  end

  def show
    # @resource = Page.find params[:id]
    build_breadcrumbs

    other_pages_relation = @resource.menu_item.nil? ? Page.detached.without(@resource.id) : @resource.siblings(published: true)
    @other_pages = other_pages_relation.shuffle.take 3
  end

  private


  def build_breadcrumbs
    @breadcrumbs = [root_breadcrumb]
    @breadcrumbs += @resource.menu_item.parent_list(no_root: true, breadcrumbs: true).map { |p| p.url.blank? ? url = view_context.menu_item_url(p) : url = p.url; { name: p.title, url: url } }.reverse if @resource.menu_item


    @breadcrumbs.push name: @resource.title

    @breadcrumbs
  end
end