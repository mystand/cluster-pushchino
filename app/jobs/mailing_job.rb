class MailingJob < ActiveJob::Base
  queue_as :default

  def perform(mailing, user)
    puts "MailingJob perform"
    @mailing = mailing
    @user = user
    @mailing.status = Mailing::STATUSES[:sending]
    @mailing.save
    MassMailer.start(@user, @mailing).deliver_now    
  end

  after_perform do |job|
    puts "MailingJob AFTER"
    mailing = job.arguments[0]
    user = job.arguments[1]
    mailing.done_with! user
  end
end